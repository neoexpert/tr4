package gl;

import com.neoexpert.gl.Shader;
import org.lwjgl.opengl.GL30;

import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL30.glBindFragDataLocation;

public class UIShader extends Shader {
    public int unishift;
    public int uniProjection;
    public int mTimeHandle;

    public UIShader(){}

    @Override
    public void compileShader(String vs, String fs) {
        int vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, vs);
        glCompileShader(vertexShader);
        int status = glGetShaderi(vertexShader, GL_COMPILE_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(vertexShader));
        }
        int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, fs);
        glCompileShader(fragmentShader);

        status = glGetShaderi(fragmentShader, GL_COMPILE_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(fragmentShader));
        }

        mProgramObject= glCreateProgram();
        glAttachShader(mProgramObject, vertexShader);
        glAttachShader(mProgramObject, fragmentShader);
        glBindFragDataLocation(mProgramObject, 0, "fragColor");
        glLinkProgram(mProgramObject);

        status = glGetProgrami(mProgramObject, GL_LINK_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetProgramInfoLog(mProgramObject));
        }

        unishift=glGetUniformLocation(mProgramObject, "shift");
        uniProjection = glGetUniformLocation(mProgramObject, "projection");
        mTimeHandle = GL30.glGetUniformLocation(mProgramObject, "time");
    }
}
