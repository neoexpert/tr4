#version 300 es		 			          	
precision mediump float;
uniform sampler2D u_Texture;    // The input texture.
 
in vec4 v_Color;
in vec2 v_TexCoord;
in vec3 v_Normal;
out vec4 fragColor;


void main()                                  
{
	vec4 c=v_Color;
	//c.a=0.5;
	//c.x=1.0;
	
       
	fragColor =   c*texture(u_Texture, v_TexCoord);
	if(fragColor.a<0.5&&v_Normal.x<10000.0)
		discard;
}
