#version 300 es 			 
uniform mat4 uMVPMatrix;
uniform float time;
in vec4 aPosition;
in vec4 aColor;
in vec2 aTexCoord;
in vec3 aNormal;
out vec4 v_Color;
out vec3 v_Normal;
out vec2 v_TexCoord;

void main()                  
{
	v_Normal=aNormal;
	v_Color = aColor;     
	v_TexCoord = aTexCoord;
	gl_Position = uMVPMatrix * aPosition;
	gl_Position.x+=sin(time);
}
