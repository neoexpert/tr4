package com.neoexpert.gl;

import com.neoexpert.math.Matrix;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Random;

public class GLMesh extends ADrawable {
	public static final int POSITION_COORDS=3;
	public static final int NORMAL_COORDS=3;
	public static final int COLOR_COORDS=4;
	public static final int TEX_COORDS=2;
	public static final int COORDS_PER_VERTEX=POSITION_COORDS+NORMAL_COORDS+COLOR_COORDS+TEX_COORDS;
	public static final int VERTEX_STRIDE=COORDS_PER_VERTEX*4;
	protected FloatBuffer mVertices;
	protected ShortBuffer drawOrder;
	private int textid;
	protected final int primitive;

	public GLMesh(NeoShader shader, FloatBuffer vertices, ShortBuffer indices){
		this.shader=shader;
		this.mVertices=vertices;
		this.drawOrder=indices;
		primitive = GL30.GL_TRIANGLES;
	}

	public GLMesh(FloatBuffer vertices, ShortBuffer indices){
		this(vertices,indices,GL30.GL_TRIANGLES);
	}

	public GLMesh(FloatBuffer vertices, ShortBuffer indices, int primitive) {
		vertices.position(0);
		indices.position(0);
		this.mVertices=vertices;
		this.drawOrder=indices;
		this.primitive = primitive;
	}

	public GLMesh(int primitive) {
	    this.primitive=primitive;
	}

	protected void setIndices(ShortBuffer indices) {
		indices.position(0);
		this.drawOrder=indices;
	}

	protected void setVertices(FloatBuffer vertices) {
		vertices.position(0);
		this.mVertices=vertices;
	}

	long startTime=System.currentTimeMillis();
	@Override
	public void draw(float[] mvpMatrix) {
		if(mVertices.capacity()<1)
			return;
		// Use the program object
		GL30.glUseProgram(shader.mProgramObject);
		GL30.glLineWidth(10f);
		//GL30.glUniform1f(shader.mTimeHandle,(float) (System.currentTimeMillis()-startTime)/1000f);

		GLRenderer.checkGlError();

		// Apply the projection and view transformation
		GL30.glUniformMatrix4fv(shader.mMVPMatrixHandle, false, mvpMatrix);
		GLRenderer.checkGlError();

		mVertices.position(0);  //just in case.  We did it already though.

		GL30.glVertexAttribPointer(shader.mPositionHandle, POSITION_COORDS, GL30.GL_FLOAT,
				true, VERTEX_STRIDE, mVertices);
		GLRenderer.checkGlError();
		GL30.glEnableVertexAttribArray(shader.mPositionHandle);

		GLRenderer.checkGlError();
		mVertices.position(3);
		GL30.glVertexAttribPointer(shader.mNormalHandle, NORMAL_COORDS, GL30.GL_FLOAT,
				true, VERTEX_STRIDE, mVertices);
		GLRenderer.checkGlError();
		GL30.glEnableVertexAttribArray(shader.mNormalHandle);

		GLRenderer.checkGlError();
		mVertices.position(6);
		GL30.glVertexAttribPointer(shader.mColorHandle, COLOR_COORDS, GL30.GL_FLOAT,
				true, VERTEX_STRIDE, mVertices);
		GL30.glEnableVertexAttribArray(shader.mColorHandle);
		GLRenderer.checkGlError();
		mVertices.position(10);  //just in case.  We did it already though.

		GL30.glVertexAttribPointer(shader.mTexCoordHandle, TEX_COORDS, GL30.GL_FLOAT,
				true, VERTEX_STRIDE, mVertices);
		GLRenderer.checkGlError();
		GL30.glEnableVertexAttribArray(shader.mTexCoordHandle);
		GLRenderer.checkGlError();


		GL30.glBindTexture(GL11.GL_TEXTURE_2D,textid);
		preDraw();
		GL30.glDrawElements(primitive, drawOrder);
		postDraw();
	}

	protected void preDraw() {
	}

	protected void postDraw() {
	}


	@Override
	public void rotateY(float angle) {
		Matrix.rotateM(modelMatrix,0,angle,0f,1f,0f);
	}

	@Override
	public void translate(float x, float y, float z) {
		Matrix.translateM(modelMatrix,0,x,y,z);
	}

	public void setTextid(int textid) {
		this.textid = textid;
	}
}
