package com.neoexpert.gl;

import org.lwjgl.opengl.GL30;

public class NeoShader extends Shader
{
	public int mTimeHandle;

	public NeoShader(String vs, String fs){
		super(vs,fs);
	}

	public NeoShader(int shaderProgram) {
	    this.mProgramObject=shaderProgram;
	}

	@Override
	public void compileShader(String vs, String fs) {
		int vertexShader;
		int fragmentShader;
		int[] linked = new int[1];

		// Load the vertex/fragment shaders
		vertexShader = loadShader(GL30.GL_VERTEX_SHADER, vs);


		fragmentShader = loadShader(GL30.GL_FRAGMENT_SHADER, fs);

		// Create the program object
		mProgramObject = GL30.glCreateProgram();
		if (mProgramObject == 0) {
			throw new RuntimeException();
		}

		GL30.glAttachShader(mProgramObject, vertexShader);
		GL30.glAttachShader(mProgramObject, fragmentShader);

		// Bind vPosition to attribute 0
		GL30.glBindAttribLocation(mProgramObject, 0, "vPosition");

		// Link the program
		GL30.glLinkProgram(mProgramObject);

		// Check the link status
		GL30.glGetProgramiv(mProgramObject, GL30.GL_LINK_STATUS, linked);

		if (linked[0] == 0) {
			StringBuilder sb=new StringBuilder();
			sb.append("Error linking program:\n");
			sb.append(GL30.glGetProgramInfoLog(mProgramObject));

			GL30.glDeleteProgram(mProgramObject);
			throw new RuntimeException(sb.toString());
		}

		// Store the program object

		mPositionHandle = 	GL30.glGetAttribLocation(mProgramObject, "aPosition");
		mColorHandle = 		GL30.glGetAttribLocation(mProgramObject, "aColor");
		mTexCoordHandle = 	GL30.glGetAttribLocation(mProgramObject, "aTexCoord");

		mNormalHandle = 	GL30.glGetAttribLocation(mProgramObject, "aNormal");

		mMVPMatrixHandle = GL30.glGetUniformLocation(mProgramObject, "uMVPMatrix");
		mTimeHandle = GL30.glGetUniformLocation(mProgramObject, "time");
		//mTextureUniformHandle = GL30.glGetUniformLocation(mProgramObject, "u_Texture");
	}

	public static int loadShader(int type, String shaderSrc) {
        int shader=0;
        int[] compiled = new int[1];

        // Create the shader object
        shader = GL30.glCreateShader(type);

        if (shader == 0) {
            return 0;
        }

        // Load the shader source
        GL30.glShaderSource(shader, shaderSrc);

        // Compile the shader
        GL30.glCompileShader(shader);

        // Check the compile status
        GL30.glGetShaderiv(shader, GL30.GL_COMPILE_STATUS, compiled);

        if (compiled[0] == 0) {
			StringBuilder sb=new StringBuilder();
			sb.append(GL30.glGetShaderInfoLog(shader));
			sb.append("\n");
			sb.append(shaderSrc);
			
            throw new RuntimeException(sb.toString());
        }

        return shader;
    }
}
