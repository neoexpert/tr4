package com.neoexpert.gl;


import com.neoexpert.math.Matrix;

public abstract class ADrawable
{
    public ADrawable(){
		Matrix.setIdentityM(modelMatrix,0);
	}
	public float[] modelMatrix=new float[16];
	public float x,y,z;
	protected NeoShader shader;

	public abstract void draw(float[] mvpMatrix);
	public abstract void rotateY(float angle);
	public abstract void translate(float x, float y, float z);

    public void setShader(NeoShader shader){
    	this.shader=shader;
	}
}
