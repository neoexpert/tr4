package com.neoexpert.gl;

import com.neoexpert.math.Vec3;
import com.neoexpert.mesh.Frustum;
import com.neoexpert.mesh.MeshBuilder;
import com.neoexpert.mesh.PlaneMesh;
import com.neoexpert.rendering.Portal;
import org.lwjgl.opengl.GL30;

public class PortalTest {
    Renderer renderer;
    private Frustum portalTest(NeoShader shader) {
	/*
        Frustum f1;
        Frustum f2;
        Frustum f3;
        {
            Portal p1=new Portal(
                    new Vec3(400,0,-1500),
                    new Vec3(0,0,-1500),
                    new Vec3(400,-100,-1500),
                    new Vec3(0,-100,-1500));
            p1.setColor(1,0,0);
            MeshBuilder mb = p1.getMesh();
            GLMesh p1m=new GLMesh(mb.genVertices(),mb.genIndices(), GL30.GL_LINES);
            p1m.setShader(shader);
            renderer.addDrawable(p1m);

            Portal p3=new Portal(
                    new Vec3(-200,-250,-500),
                    new Vec3(-300,-250,-500),
                    new Vec3(-200,-350,-500),
                    new Vec3(-300,-350,-500));
            p3.setColor(0,1,0);
            mb = p3.getMesh();
            GLMesh p3m=new GLMesh(mb.genVertices(),mb.genIndices(), GL30.GL_LINES);
            p3m.setShader(shader);
            renderer.addDrawable(p3m);


            //Portal p2=new Portal(new Vec3(0,100,-500),new Vec3(100,100,-500),new Vec3(0,0,-500),new Vec3(100,0,-500));
            Portal p2=new Portal(
                    new Vec3(100,-100,-1000),
                    new Vec3(0,-100,-1000),
                    new Vec3(100,-200,-1000),
                    new Vec3(0,-200,-1000));
            p2.setColor(0,0,1);
            mb = p2.getMesh();
            GLMesh p2m=new GLMesh(mb.genVertices(),mb.genIndices(), GL30.GL_LINES);
            p2m.setShader(shader);
            renderer.addDrawable(p2m);

            f1=p1.genFrustum(p2);
            mb=f1.buildLined();
            GLMesh frustumMesh=new GLMesh(mb.genVertices(),mb.genIndices(),GL30.GL_LINES);
            frustumMesh.setShader(shader);
            //renderer.addDrawable(frustumMesh);

            f3 = f1.reduceTo(p3);
            mb=f3.buildLined();
            GLMesh frustumMesh3=new GLMesh(mb.genVertices(),mb.genIndices(),GL30.GL_LINES);
            frustumMesh3.setShader(shader);
            renderer.addDrawable(frustumMesh3);

            f2=p2.genFrustum(p3);



            mb=f2.buildLined();
            GLMesh frustumMesh2=new GLMesh(mb.genVertices(),mb.genIndices(),GL30.GL_LINES);
            frustumMesh2.setShader(shader);
            //renderer.addDrawable(frustumMesh2);

            mb= PlaneMesh.genPlaneMeshXZ(f3.bottomPlane,1,1,0);
            GLMesh planeMesh=new GLMesh(mb.genVertices(),mb.genIndices(),GL30.GL_LINES);
            planeMesh.setShader(shader);
            //renderer.addDrawable(planeMesh);

            mb=PlaneMesh.genPlaneMeshXZ(f2.bottomPlane,0,1,1);
            planeMesh=new GLMesh(mb.genVertices(),mb.genIndices(),GL30.GL_LINES);
            planeMesh.setShader(shader);
            //renderer.addDrawable(planeMesh);
        }
        return f3;
	*/
		return null;
    }
}
