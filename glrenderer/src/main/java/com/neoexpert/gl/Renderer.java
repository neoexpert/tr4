package com.neoexpert.gl;


import com.neoexpert.mesh.Frustum;
import com.neoexpert.rendering.Camera;

public interface Renderer {
    void addDrawable(ADrawable drawable);

    void removeFromTree(ADrawable o);

    void addObjectToTree(ADrawable d);

    int getWidth();

    int getHeight();

    void addRenderer(Renderer renderer);

    void drawScene(float[] vpMatrix);

    void bindFrameBuffer();
    void frustum(float bottom, float top, float near, float far);

    void perspective(float fov, float near, float far);

    void ortho(float left, float right, float bottom, float top, float near, float far);

    void onSurfaceChanged(int width, int height);
    void onSurfaceCreated();


    void setLookAt(float eyeX, float eyeY, float eyeZ, float lookX, float lookY, float lookZ);

    void onDrawFrame();
    void setW(boolean b);
    void setA(boolean b);
    void setS(boolean b);
    void setD(boolean b);

    void setSpeed(float v);

    void drawFrustum();

    void mouseMove(float x, float y);

    void mouseDown();

    void mouseUp();

    Camera getCamera();

    void setFrustum(Frustum frustum);
}
