package com.neoexpert.gl;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;


public class GLRoomMesh extends GLMesh{
	public static final int POSITION_COORDS=3;
	public static final int NORMAL_COORDS=3;
	public static final int COLOR_COORDS=4;
	public static final int TEX_COORDS=2;
	public static final int COORDS_PER_VERTEX=POSITION_COORDS+NORMAL_COORDS+COLOR_COORDS+TEX_COORDS;
	public static final int VERTEX_STRIDE=COORDS_PER_VERTEX*4;

    public GLRoomMesh(NeoShader shader , FloatBuffer vertices, ShortBuffer indices) {
        super(shader,vertices,indices);
    }

	public GLRoomMesh(FloatBuffer vertices, ShortBuffer indices) {
		super(vertices,indices);
	}

	@Override
	public void rotateY(float angle) {

	}

	@Override
	public void translate(float x, float y, float z) {

	}
}
