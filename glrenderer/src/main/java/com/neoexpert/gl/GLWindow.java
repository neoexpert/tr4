package com.neoexpert.gl;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.Callback;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.nglfwGetFramebufferSize;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.system.MemoryUtil.NULL;
import static org.lwjgl.system.MemoryUtil.memAddress;

public class GLWindow {
    double oX=Double.NaN, oY=Double.NaN;
    public float rotX;
    public float rotY;
    private Renderer renderer;
    private long window;
    private GLFWFramebufferSizeCallback fbCallback;
    private GLFWWindowSizeCallback wsCallback;
    private GLFWKeyCallback keyCallback;
    private GLFWCursorPosCallback cpCallback;
    private GLFWScrollCallback sCallback;
    private GLCapabilities caps;
    private Callback debugProc;
    private int width;
    private int height;
    float eyeX, eyeY, eyeZ=512;
    float lookX, lookY, lookZ;
    private boolean uimode=true;

    public void init(int w, int h){
        this.width=w;
        this.height=h;
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        window = glfwCreateWindow(width, height, "TR GLRenderer", NULL, NULL);
        if (window == NULL)
            throw new AssertionError("Failed to create the GLFW window");

        System.out.println("Move the mouse to look around");
        System.out.println("Zoom in/out with mouse wheel");
        glfwSetFramebufferSizeCallback(window, fbCallback = new GLFWFramebufferSizeCallback() {
            @Override
            public void invoke(long window, int _w, int _h) {
                if (_w> 0 && _h> 0) {
                    width=_w;
                    height=_h;
                    renderer.onSurfaceChanged(_w, _h);
                }
            }
        });
        glfwSetWindowSizeCallback(window, wsCallback = new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long window, int _w, int _h) {
                if (_w> 0 && _h> 0 ) {
                    if (_w> 0 && _h> 0) {
                        width=_w;
                        height=_h;
                        renderer.onSurfaceChanged(_w, _h);
                    }
                }
            }
        });
        glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {

                switch(action) {
                    case GLFW_PRESS:
                        keyDown(key);
                        break;
                    case GLFW_RELEASE:
                        keyUp(key);
                        break;
                }


            }
        });
        glfwSetMouseButtonCallback(window,new GLFWMouseButtonCallback() {
            private long last_click;
            @Override
            public void invoke(long window, int button, int action, int mods) {
                if(action == GLFW_PRESS)
                    renderer.mouseDown();
                else
                    renderer.mouseUp();
            }
        });

        glfwSetCursorPosCallback(window, cpCallback = new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double x, double y) {
                if(!uimode) {
                    if(Double.isNaN(oX)){
                        oX=x;
                        oY=y;
                    }
                    rotX+=(x-oX)/100f;
                    rotY-=(y-oY)/100f;

                    oX=x;
                    oY=y;
                    renderer.getCamera().rotate(rotX, rotY);
                }
                renderer.mouseMove((float)x,(float)y);
            }

        });

        glfwSetScrollCallback(window, sCallback = new GLFWScrollCallback() {
            @Override
            public void invoke(long window, double xoffset, double yoffset) {
            }
        });

        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(window, (vidmode.width() - width) / 2, (vidmode.height() - height) / 2);
        glfwMakeContextCurrent(window);
        glfwSwapInterval(0);
        glfwShowWindow(window);
        glfwSetCursorPos(window, width / 2, height / 2);

        try (MemoryStack frame = MemoryStack.stackPush()) {
            IntBuffer framebufferSize = frame.mallocInt(2);
            nglfwGetFramebufferSize(window, memAddress(framebufferSize), memAddress(framebufferSize) + 4);
            width = framebufferSize.get(0);
            height = framebufferSize.get(1);
        }

        caps = GL.createCapabilities();
        if (!caps.GL_ARB_shader_objects)
            throw new AssertionError("This demo requires the ARB_shader_objects extension.");
        if (!caps.GL_ARB_vertex_shader)
            throw new AssertionError("This demo requires the ARB_vertex_shader extension.");
        if (!caps.GL_ARB_fragment_shader)
            throw new AssertionError("This demo requires the ARB_fragment_shader extension.");
        debugProc = GLUtil.setupDebugMessageCallback();

    }
    private void keyDown(int key) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                uimode=true;
                perspective=false;
                switchProjection();
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
                //glfwSetWindowShouldClose(window, false);
                break;
            case GLFW_KEY_W:
                renderer.setW(true);
                break;
            case GLFW_KEY_S:
                renderer.setS(true);
                break;
            case GLFW_KEY_A:
                renderer.setA(true);
                break;
            case GLFW_KEY_D:
                renderer.setD(true);
                break;
            case GLFW_KEY_F:
                renderer.drawFrustum();
                break;
            case GLFW_KEY_LEFT_SHIFT:
                renderer.setSpeed(1f);
                break;
            case GLFW_KEY_P:
                switchProjection();
			/*
			Vec3 p = positions.get(currentpos % positions.size());
			eyeX=-p.x/1024f;
			eyeY=-p.y/1024f+1;
			eyeZ=p.z/1024f;

			 */

                break;
            case GLFW_KEY_O:
                uimode=false;
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
                break;
            default:
                break;
        }
    }
    boolean perspective=false;
    public void switchProjection() {
        perspective=!perspective;

        if(perspective) {
            //Matrix.frustumM(projectionMatrix,0,-ratio, ratio, -1, 1, 1f, 2000);
            //renderer.frustum(-1,1,1,2000);
            renderer.perspective((float) Math.PI/4f,1,2000);
            float ratio = (float)width / height;
            float eyeZ= -(width/2f)/ratio;
            float lookX=0;
            float lookY=0.0f;
            float lookZ=1;
            renderer.setLookAt(0f,0f,eyeZ,lookX,lookY,lookZ);
            System.out.println("perspective");
        }
        else {
            renderer.ortho(-width/2, width/2, -height/2, height/2, -1000f, 1000f);
            //Matrix.setLookAtM(viewMatrix,0,eyeX, eyeY,eyeZ,eyeX+lookX,eyeY+lookY,eyeZ+lookZ,0,1,0);
            //Matrix.setIdentityM(viewMatrix,0);
            //Matrix.multiplyMM(viewProjMatrix,0,viewMatrix,0,projectionMatrix,0);
            System.out.println("orthographic");
        }
    }

    private void keyUp(int key) {
        switch (key) {
            case GLFW_KEY_ESCAPE:

                break;
            case GLFW_KEY_W:
                renderer.setW(false);
                break;
            case GLFW_KEY_S:
                renderer.setS(false);
                break;
            case GLFW_KEY_A:
                renderer.setA(false);
                break;
            case GLFW_KEY_D:
                renderer.setD(false);
                break;
            case GLFW_KEY_LEFT_SHIFT:
                renderer.setSpeed(0.1f);
                break;
            default:
                break;
        }
    }
    public void run() {
        switchProjection();
        try {
            //init();
            loop();
            if (debugProc != null) {
                debugProc.free();
            }
            cpCallback.free();
            keyCallback.free();
            fbCallback.free();
            wsCallback.free();
            glfwDestroyWindow(window);
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            glfwTerminate();
        }
    }
    void loop() {
        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents();
            glViewport(0, 0, width, height);
            renderer.onDrawFrame();
            glfwSwapBuffers(window);
        }
    }

    public Renderer getRenderer() {
        return renderer;
    }

    public void setRenderer(Renderer renderer) {
        this.renderer = renderer;
    }
}
