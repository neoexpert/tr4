package com.neoexpert.gl;

import com.neoexpert.tr4.level.Level;
import com.neoexpert.tr4.level.Texture;
import com.neoexpert.tr4.level.animation.TRAnimation;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.mesh.TRMeshBuilder;
import com.neoexpert.tr4.mesh.TRRoomMeshBuilder;
import com.neoexpert.tr4.rendering.texture.TRTexture;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;


public class TRRenderer {
    private GLRenderer renderer;
    private NeoShader shader;

    public static void main(String[] args) throws IOException {
        new TRRenderer().run();
    }

    public static String ioResourceToString(String resource) throws IOException {
        URL url = Thread.currentThread().getContextClassLoader().getResource(resource);
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line)
                    .append("\n");
        }
        System.out.println(out.toString());   //Prints the string content read from input stream
        reader.close();
        return out.toString();
    }
    private void run() throws IOException {
        GLWindow window=new GLWindow();
        renderer = new GLRenderer();
        window.setRenderer(renderer);
        renderer.perspective((float) (60*(Math.PI/360f)),0.1f,1000f);
        window.init(1024,768);
        try {
            shader = new NeoShader(ioResourceToString("vsc.glsl"), ioResourceToString("fsc.glsl"));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        loadLevel("levels/test.tr4");
        window.run();
    }

    void loadLevel(String file) throws IOException {
        FileInputStream fin = new FileInputStream(file);
        Level l = Level.read(fin);
        Entity lara = l.levelData.lara;
        System.out.println(lara);
	renderer.getCamera().setPos(-lara.x/1024f,-lara.y/1024f+1,lara.z/1024f);

        int tid = renderer.createTexture(createTexture(l.textures32));
        int[] _at=l.levelData.animatedTextures;
        Set<Integer> at=new HashSet<>();
        for (int i=0;i < _at.length;i++)
        {
            at.add(_at[i]);
        }
        //synchronized (drawables)
        {
            //rooms=new TRRoomMeshBuilder[1];
            for (int i = 0; i < l.levelData.rooms.length; i++) {
                Room room = l.levelData.rooms[i];
                TRRoomMeshBuilder rmb=new TRRoomMeshBuilder(room,l,at);
                GLRoomMesh trroom = new GLRoomMesh(rmb.roomMesh.genVertices(), rmb.roomMesh.genIndices());
                trroom.setTextid(tid);
                trroom.setShader(shader);
                renderer.addDrawable(trroom);
                //drawables.add();
                //drawables.add(new TRRoomGL(room, l, shader, this));
                for (RoomStaticMesh rsm : room.staticMeshes) {
                    StaticMesh smf = null;
                    for (StaticMesh sm : l.levelData.staticMeshes) {
                        if (rsm.meshID == sm.id)
                            smf = sm;
                    }

                    com.neoexpert.tr4.level.mesh.Mesh mesh = l.levelData.meshes.get(l.levelData.meshPointers[smf.mesh]);

                    TRMeshBuilder tmb=new TRMeshBuilder(mesh,l);
                    GLMesh m = new GLMesh(tmb.getVertices(), tmb.getIndices());
                    m.setShader(shader);
                    m.setTextid(tid);
                    //TRMesh m=new GLMesh(mesh, l, shader, this);

                    m.translate(-rsm.x / 1024f, -rsm.y / 1024f, rsm.z / 1024f);

                    float angle = (rsm.rotation / 16384) * -90;
                    // logger.log(angle+"");
                    m.rotateY((float) (angle/180*Math.PI));
                    renderer.addDrawable(m);
                }
            }
            Entity[] items=l.levelData.items;

            Model[] models=l.levelData.moveables;
            Model f=null;
            //for (int j=193;j<197;j+=3)
            for (int j=0;j<items.length;j++)
            {
                Entity item=items[j];

                //Entity item=l.levelData.lara;
                for (Model m:models)
                {
                    if (m.id == item.typeID)
                    {
                        f = m;

                        //break;
                    }
                }
                TRAnimation a=l.levelData.animations[f.animation];
                //Frame frame=l.levelData.getFrame(a.frameOffset/2);


                Room r=l.levelData.rooms[item.room];
                //logger.log(f.numMeshes+"");
                //logger.log("meshes: "+l.levelData.meshes.size()+"");
                //logger.log("meshesp: "+l.levelData.meshPointers.length+"");
                ///*

                com.neoexpert.tr4.level.mesh.Mesh mesh= l.levelData.meshes.get(l.levelData.meshPointers[f.startingMesh]);

                if(mesh.flags==1)
                    continue;

                //logger.log(mesh.toString());

                //logger.log("meshtree:"+f.meshTree);
                TRMeshBuilder tmb=new TRMeshBuilder(mesh,l);
                GLMesh m = new GLMesh(tmb.getVertices(), tmb.getIndices());
                m.setShader(shader);
                m.setTextid(tid);
                //TRMesh m=new GLMesh(mesh, l, shader, this);

                int dx=0,dy=0,dz=0,mt;
                if (f.meshTree < l.levelData.meshTrees.length)
                {
                    mt = l.levelData.meshTrees[f.meshTree];
                    dx = l.levelData.meshTrees[f.meshTree + 1];
                    dy = l.levelData.meshTrees[f.meshTree + 2];
                    dz = l.levelData.meshTrees[f.meshTree + 3];
                }

                m.translate((-item.x) / 1024f,(-item.y) / 1024f, (item.z) / 1024f);
                //int angle=(item.angle / 16384) * -90;
                float angle = (item.angle / 16384) * -90;

                //logger.log(angle+"");
                m.rotateY((float) (angle/180*Math.PI));

                //mod.stack.push(m);
                GLMesh cp=m;
                GLMesh last_mesh=cp;
                renderer.addDrawable(m);
                //int stack=0;
                Stack<GLMesh> stack=new Stack<>();
                for (int i=1;i < f.numMeshes;i++)
                {

                    mesh= l.levelData.meshes.get(l.levelData.meshPointers[f.startingMesh + i]);

                    tmb=new TRMeshBuilder(mesh,l);
                    m = new GLMesh(tmb.getVertices(), tmb.getIndices());
                    m.setShader(shader);
                    m.setTextid(tid);
                    //m=new GLMesh(mesh, l, shader, this);


                    //logger.log(m+"");
                    //int dx=0,dy=0,dz=0;

                    mt = l.levelData.meshTrees[f.meshTree + 0 + (i-1) * 4];
                    dx = l.levelData.meshTrees[f.meshTree + 1 + (i-1) * 4];
                    dy = l.levelData.meshTrees[f.meshTree + 2 + (i-1) * 4];
                    dz = l.levelData.meshTrees[f.meshTree + 3 + (i-1) * 4];


                    //if (stack < 0)break;
                    //logger.log(dy+"\n");


                    if (((mt >> 0) & 1) == 1)
                    {
                        //stack--;
                        cp=stack.pop();
                    }
                    else
                        cp=last_mesh;

                    if (((mt >> 1) & 1) == 1)
                    {
                        stack.push(cp);
                        //stack++;
                    }


                    m.translate(cp.x+ (-dx) / 1024f,cp.y+ ( -dy) / 1024f, cp.z+ (+ dz) / 1024f);

                    //int angle=(item.angle / 16384) * -90;
                    //logger.log(angle+"");
                    //m.rotateY((float) (angle/180*Math.PI));
                    renderer.addDrawable(m);
                    last_mesh=m;
                }
                //*/
                f=null;
            }

        }

    }
    public static class MyBufferedImage implements TRTexture.ABitmap {
        public BufferedImage img;
        @Override
        public void setPixel(int x, int y, int c) {
            img.setRGB(x, y, c);
        }
        @Override
        public void createBitmap(int w, int h) {
            this.img = new BufferedImage(w, h, BufferedImage.TYPE_4BYTE_ABGR);
        }
    }
    static BufferedImage createTexture(Texture[] t) throws IOException {
        MyBufferedImage img=new MyBufferedImage();
        TRTexture.loadTRTexture(t,img);
        return img.img;
    }
}
