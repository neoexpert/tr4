package com.neoexpert.gl;

import com.neoexpert.mesh.*;
import com.neoexpert.rendering.*;
import com.neoexpert.math.Matrix;
import org.lwjgl.BufferUtils;
import com.neoexpert.Logger;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

/**
 *
 */
public class GLRenderer implements Renderer, Logger {
	private ArrayList<ADrawable> drawables = new ArrayList<>();
	private boolean w=false,s=false, a=false, d=false;
	long window;
	int width = 1024;
	int height = 768;
	float fov = (float) (60*(Math.PI / 360.0));
	private final Camera cam=new Camera();
	private float near;
	private float far;

	public GLRenderer(){
		cam.setUp(0,1,0);
		cam.setPos(0,0,-512);
	}
	@Override
	public Camera getCamera(){
		return cam;
	}

	@Override
	public void setFrustum(Frustum frustum) {
		this.frustum=frustum;
		MeshBuilder mb = frustum.buildLined();
		frustumMesh.setVertices(mb.genVertices());
		frustumMesh.setIndices(mb.genIndices());
	}

	float speed=0.1f;
	float[] mvpMatrix=new float[16];
	private List<Renderer> childRenderer=new ArrayList<>();
	private Frustum frustum=new Frustum();
	private GLMesh frustumMesh;


	@Override
	public void frustum(float bottom, float top, float near, float far){
		float ratio = (float)width / height;
		cam.frustum(ratio,bottom,top,near,far);
	}

	@Override
	public void perspective(float fov, float near, float far){
		this.near=near;
		this.far=far;
		this.fov=fov;
		float ratio = (float)width / height;
		cam.perspective(ratio,fov,near,far);
	}
	@Override
	public void ortho(float left, float right, float bottom, float top, float near, float far){
		cam.ortho(left,right,bottom,top,near,far);
	}

	@Override
	public void onSurfaceChanged(int width, int height) {
	    this.width=width;
	    this.height=height;
	    for(Renderer r:childRenderer)
	    	r.onSurfaceChanged(width,height);
		glViewport(0, 0, width, height);
		float ratio = (float)width / height;
		cam.perspective(ratio,fov,near,far);
	}

	@Override
	public void onSurfaceCreated() {
		frustumMesh=new GLMesh(GL_LINES);
		refreshFrustum();
	}


	@Override
	public void setLookAt(float eyeX, float eyeY, float eyeZ, float lookX, float lookY, float lookZ) {
	    	cam.lookAt(eyeX,eyeY,eyeZ,lookX,lookY,lookZ);
	}



	public FrameBuffer createFrameBufer(int width, int height) {
		return new FrameBuffer(width,height);
	}

	public void addDrawable(ADrawable d) {
		drawables.add(d);
	}

	@Override
	public void removeFromTree(ADrawable o) {

	}

	@Override
	public void addObjectToTree(ADrawable d) {

	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public void addRenderer(Renderer renderer) {
		this.childRenderer.add(renderer);
		renderer.setFrustum(frustum);
	}

	@Override
	public void drawScene(float[] vpMatrix) {

	}

	@Override
	public void bindFrameBuffer() {
		glBindFramebuffer(GL_FRAMEBUFFER,0 );
	}


	public int createTexture(BufferedImage img) throws IOException {
		int tex = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, tex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		File outputfile = new File("image.png");
		ImageIO.write(img, "png", outputfile);


		//gltexImage2D(GL_TEXTURE_2D, 0, convertImageData(img), 0);
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w.get(0), h.get(0), 0, GL_RGBA, GL_UNSIGNED_BYTE, convertImageData(img));
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.getWidth(), img.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE,  convertImageData(img));

		return tex;
	}

	private static ByteBuffer convertImageData(BufferedImage image) {
		int width = image.getWidth(), height = image.getHeight();

		int[] pixels = new int[width * height];
		image.getRGB(0, 0, width, height, pixels, 0, width);

		ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * 4); // 4 because RGBA

		for(int y = 0; y < height; ++y) {
		    for(int x = 0; x < width; ++x) {
		        int pixel = pixels[x + y * width];
		        buffer.put((byte) ((pixel >> 16) & 0xFF));
		        buffer.put((byte) ((pixel >> 8) & 0xFF));
		        buffer.put((byte) (pixel & 0xFF));
		        buffer.put((byte) ((pixel >> 24) & 0xFF));
		    }
		}

		buffer.flip();
	    return buffer;
	}


	public void onDrawFrame() {
		update();
		//glClearColor(0,0,0,1.0f);
		//glViewport(0, 0, fbWidth, fbHeight);
		//glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
        	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		for(Renderer r:childRenderer)
			renderChild(r);

		synchronized(drawables) {
			for (ADrawable t : drawables) {

				Matrix.multiplyMM(mvpMatrix,0,t.modelMatrix,0,cam.viewProjMatrix,0);
				t.draw(mvpMatrix);
			}
		}
		if(drawFrustum)
			refreshFrustum();
		//Matrix.multiplyMM(mvpMatrix,0,frustumMesh.modelMatrix,0,cam.viewProjMatrix,0);
		//frustumMesh.draw(mvpMatrix);
	}


	@Override
	public void setW(boolean b) {
		w=b;
	}

	@Override
	public void setA(boolean b) {
		a=b;
	}

	@Override
	public void setS(boolean b) {
		s=b;
	}

	@Override
	public void setD(boolean b) {
		d=b;
	}

	@Override
	public void setSpeed(float v) {
		this.speed=v;
	}

	public void update() {
		//viewMatrix.originAffine(cameraPosition);
		if(w) {
			cam.moveForwards(speed);
		}
		if(s) {
			cam.moveBackwards(speed);
		}

		if(a) {
			cam.moveLeftwards(speed);
		}
		if(d) {
			cam.moveRightwards(speed);
		}
	}

	private void renderChild(Renderer childRenderer) {
		//glClearColor(1,1,0,0.0f);
		//childRenderer.bindFrameBuffer();
		//glViewport(0, 0, fbWidth, fbHeight);
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
		glEnableClientState(GL_VERTEX_ARRAY);
		childRenderer.drawScene(cam.viewProjMatrix);
		glBindVertexArray(0);
	}

	void loop() {
		while (!glfwWindowShouldClose(window)) {
			glfwPollEvents();
			//render();
			glfwSwapBuffers(window);
		}
	}


	public static void checkGlError() {
	}


	@Override
	public void log(String s) {
		// TODO Auto-generated method stub
	}
	public void genFrustumMesh(NeoShader shader){
		frustumMesh=new GLMesh(GL_LINES);
		frustumMesh.setShader(shader);
	}


	boolean drawFrustum=false;
	@Override
	public void drawFrustum() {
		drawFrustum=!drawFrustum;
	}

	@Override
	public void mouseMove(float x, float y) {
		for(Renderer r:childRenderer)
			r.mouseMove(x,y);
	}

	@Override
	public void mouseDown(){
		for(Renderer r:childRenderer)
			r.mouseDown();
	}

	@Override
	public void mouseUp(){
		for(Renderer r:childRenderer)
			r.mouseUp();
	}

	/*
	Comment for: http://www.lighthouse3d.com/tutorials/view-frustum-culling/geometric-approach-extracting-the-planes/
	There is a Problem: this works only if the frustum is rotated in XZ plane. Because the up-vector is always pointing upward and does not rotate. So with
    ftl = fc + (up * Hfar/2) - (right * Wfar/2)
    you'll get a point on a plane which is parallel to the up vector. So the resulting planes must be parallel to the y-axis. How to solve that?
     */
	public void refreshFrustum(){
		MeshBuilder mb=frustum.build(cam);
		frustumMesh.setVertices(mb.genVertices());
		frustumMesh.setIndices(mb.genIndices());
	}
	/*
	void extract_planes_from_projmat(
        const float mat[4][4],
		float left[4], float right[4], float top[4], float bottom[4],
		float near[4], float far[4])
	{
		for (int i = 4; i--; ) left[i]      = mat[i][3] + mat[i][0];
		for (int i = 4; i--; ) right[i]     = mat[i][3] - mat[i][0];
		for (int i = 4; i--; ) bottom[i]    = mat[i][3] + mat[i][1];
		for (int i = 4; i--; ) top[i]       = mat[i][3] - mat[i][1];
		for (int i = 4; i--; ) near[i]      = mat[i][3] + mat[i][2];
		for (int i = 4; i--; ) far[i]       = mat[i][3] - mat[i][2];
	}

	 */
}
