package com.neoexpert.gl;

import com.neoexpert.*;
import java.io.*;


public abstract class Shader {
	public int mProgramObject;
	public int mPositionHandle;
	public int mMVPMatrixHandle;
	public int mColorHandle;
	public int mTexCoordHandle;
    public int mNormalHandle;
	private int mTextureUniformHandle;

	public Shader(String vs, String fs){
		compileShader(vs,fs);
	}

	protected Shader(){}

	public abstract void compileShader(String vs, String fs);
}
