package com.neoexpert.tr4.mesh;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class AMesh{
    private final FloatBuffer vertices;
    private final ShortBuffer indices;

    public AMesh(FloatBuffer vertices, ShortBuffer indices) {
        this.vertices=vertices;
        this.indices=indices;
    }

    public FloatBuffer getVertices() {
        return vertices;
    }

    public ShortBuffer getIndices() {
        return indices;
    }
}
