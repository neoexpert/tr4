package com.neoexpert.tr4.mesh;
import com.neoexpert.mesh.*;

import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.room.*;

import java.util.*;
import com.neoexpert.math.*;


public class TRRoomMeshBuilder
{

    public MeshBuilder roomMesh;
	public MeshBuilder waterMesh;
	public MeshBuilder transparencyMesh;
	public MeshBuilder doubleSidedTMPMesh;
	public MeshBuilder doubleSidedTransparentTMPMesh;
	private boolean hasVertices;
    
    public TRRoomMeshBuilder(Room r, Level l, Set<Integer> at) {
		boolean iswaterroom=r.isFilledWithWater();
		int ytop=r.room_info.yTop;

		RoomInfo room_info=r.room_info;

		//room_info.yBottom=200;
		RoomVertex[] verts=r.roomVertices;
		Face4[] rects=r.rects;
		Face3[] triangles=r.triangles;
		//rects=new Face4[1];

		int rectscount=1;
		int tricount=0;
		rectscount=rects.length;
		tricount=triangles.length;
		int meshSize=(rectscount*4+3*tricount)*(3+4+2);
		
		
		//Integer roomfi=0;
		roomMesh=new MeshBuilder(meshSize,tricount*3+ rectscount*6);
		waterMesh=null;
		transparencyMesh=null;
		doubleSidedTMPMesh=null;
		doubleSidedTransparentTMPMesh=null;
		//Integer waterfi=0;
		
		//short fi=0;
		TRLight[] lights=r.lights;
		//logger.log("room_lights: "+lights.length);
		for(TRLight light:lights){
			if(light instanceof TR4Light){
				TR4Light tr4l=(TR4Light)light;
				//logger.log(" type:" + tr4l.type);
			}
		}
		
		int twidth=(int)Math.sqrt(l.textures32.length)+1;

		for(int j=0;j< rectscount;j++){
			Face4 f=rects[j];
			ObjectTexture ot=l.levelData.objectTextures[f.getTexture()];
			
			
			int t=ot.getTile();
			int tx=t%twidth;
			int ty=t/twidth;
			
			
			MeshBuilder currentMesh=null;
		
			//Integer fi=roomfi;
			
			if (ot.attr == 512)
			{
				//if texture is transparent and animated and the mesh is parallel to XZ plane, it is probably water
				if (at.contains(f.getTexture()) && verts[f.v0].vertex.y == verts[f.v1].vertex.y && verts[f.v1].vertex.y == verts[f.v2].vertex.y && verts[f.v2].vertex.y == verts[f.v3].vertex.y)
				{
					if(waterMesh==null)
						waterMesh=new MeshBuilder();
					currentMesh = waterMesh;
				
				}
				

			}
			if (currentMesh == null)
				if (ot.attr == 512&& f.isDoubleSided())
				{
					if (doubleSidedTransparentTMPMesh == null)
						doubleSidedTransparentTMPMesh = new MeshBuilder();

					currentMesh = doubleSidedTransparentTMPMesh;
				}
			if (currentMesh == null)
				if (f.isDoubleSided())
				{
					if (doubleSidedTMPMesh == null)
						doubleSidedTMPMesh = new MeshBuilder();

					currentMesh = doubleSidedTMPMesh;
				}
			if (currentMesh == null)
			{
				if (roomMesh == null)
					roomMesh = new MeshBuilder();
				hasVertices=true;
				currentMesh = roomMesh;
			}
			//int[] tt=l.texttiles32[t];
			
			//v0
			currentMesh.addIndex((short)(currentMesh.fi+0));
			currentMesh.addIndex((short)(currentMesh.fi+1));
			currentMesh.addIndex((short)(currentMesh.fi+2));
			currentMesh.addIndex((short)(currentMesh.fi+0));
			currentMesh.addIndex((short)(currentMesh.fi+2));
			currentMesh.addIndex((short)(currentMesh.fi+3));
			currentMesh.fi+=4;

				
			//XYZ
			Vec3 pos=new Vec3();
			pos.x=(room_info.x + verts[f.v0].vertex.x) / -1024.0f;
			pos.y=( -verts[f.v0].vertex.y)/1024.0f;
			pos.z=((room_info.z+verts[f.v0].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color rgba
			float avgl=((verts[f.v0].getR() +verts[f.v0].getG() + verts[f.v0].getB())/3f)/31f;
			
			Vec4 color=new Vec4();
			color.x= avgl;//verts[f.v0].getR() / 31.0f;
			
			color.y=avgl;// verts[f.v0].getG() / 31.0f;
			
			color.z=avgl;// verts[f.v0].getB() / 31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			
//			//normals
//			roomMesh.add(1.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			//textcoord
			Vec2 texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v0.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v0.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);
			

			pos=new Vec3();
			//XYZ
			pos.x=((room_info.x+verts[f.v1].vertex.x)/-1024.0f);
			
			pos.y=(( -verts[f.v1].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v1].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color rgba
			avgl=((verts[f.v1].getR() +verts[f.v1].getG() + verts[f.v1].getB())/3f)/31f;
			
			color=new Vec4();
			color.x=(avgl); // verts[f.v1].getR()/31.0f;
			
			color.y=(avgl);// verts[f.v1].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v1].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			//normals
//			roomMesh.add(1.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v1.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v1.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);

			pos=new Vec3();
			//XYZ
			pos.x=((room_info.x+verts[f.v2].vertex.x)/-1024.0f);
			
			pos.y=(( - verts[f.v2].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v2].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color rgba
			avgl=((verts[f.v2].getR() +verts[f.v2].getG() + verts[f.v2].getB())/3f)/31f;
			color=new Vec4();
			color.x=(avgl); //verts[f.v2].getR()/31.0f;
			
			color.y=(avgl); //verts[f.v2].getG()/31.0f;
			
			color.z=(avgl); //verts[f.v2].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			//normals
//			roomMesh.add(1.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
			
			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v2.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v2.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);

			//XYZ
			pos=new Vec3();
			pos.x=((room_info.x+verts[f.v3].vertex.x)/-1024.0f);
			
			pos.y=(( -verts[f.v3].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v3].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color rgba
			avgl=((verts[f.v3].getR() +verts[f.v3].getG() + verts[f.v3].getB())/3f)/31f;
			
			color=new Vec4();
			color.x=(avgl); // verts[f.v3].getR()/31.0f;
			
			color.y=(avgl);// verts[f.v3].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v3].getB()/31.0f;
			
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			
			//normals
//			roomMesh.add(1.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v3.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v3.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);
		}
	
		
		//fi=0;
		for(int j=0;j< tricount;j++){

			Face3 f=triangles[j];
			ObjectTexture ot=l.levelData.objectTextures[f.getTexture()];
			int t=ot.getTile();
			int tx=t%twidth;
			int ty=t/twidth;
			MeshBuilder currentMesh=roomMesh;

			//Integer fi=roomfi;

			if (ot.attr == 512)
			{
				//if texture is transparent and animated and the mesh is parallel to XZ plane, it is probably water
				if (at.contains(f.getTexture()) && verts[f.v0].vertex.y == verts[f.v1].vertex.y && verts[f.v1].vertex.y == verts[f.v2].vertex.y)
				{
					if(waterMesh==null)
						waterMesh=new MeshBuilder();
					currentMesh = waterMesh;

				}


			}
			if ( currentMesh == null)
				if (ot.attr == 512&& f.isDoubleSided())
				{
					if (doubleSidedTransparentTMPMesh == null)
						doubleSidedTransparentTMPMesh = new MeshBuilder();

					currentMesh = doubleSidedTransparentTMPMesh;
				}
			if (currentMesh == null)
				if (f.isDoubleSided())
				{
					if (doubleSidedTMPMesh == null)
						doubleSidedTMPMesh = new MeshBuilder();

					currentMesh = doubleSidedTMPMesh;
				}
			if (currentMesh == null)
			{
				if (roomMesh == null)
					roomMesh = new MeshBuilder();
				hasVertices=true;
				currentMesh = roomMesh;
			}	
			
			//v0
			
			currentMesh.addIndex((short)(currentMesh.fi+0));
			currentMesh.addIndex((short)(currentMesh.fi+1));
			currentMesh.addIndex((short)(currentMesh.fi+2));
			currentMesh.fi+=3;



			//XYZ
			Vec3 pos=new Vec3();
			pos.x=((room_info.x+verts[f.v0].vertex.x)/-1024.0f);
			
			pos.y=(( -verts[f.v0].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v0].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color argb
			Vec4 color=new Vec4();
			float avgl=((verts[f.v0].getR() + verts[f.v0].getG() + verts[f.v0].getB()) / 3f) / 31f;
			color.x=(avgl);// //verts[f.v0].getR() / 31.0f;
			
			color.y=(avgl);// verts[f.v0].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v0].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
//			//normals
//			roomMesh.add(1.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			//textcoord
			Vec2 texcoord=new Vec2();
			texcoord.x=((tx * 256 + (ot.v0.xPixel)) / (twidth * 256.0f));

			texcoord.y=((ty*256+(ot.v0.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);
			
			//XYZ
			pos=new Vec3();
			pos.x=((room_info.x+verts[f.v1].vertex.x)/-1024.0f);
			
			pos.y=(( -verts[f.v1].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v1].vertex.z)/1024.0f);
			
			currentMesh.addPosition(pos);
			//color argb
			avgl=((verts[f.v1].getR() + verts[f.v1].getG() + verts[f.v1].getB()) / 3f) / 31f;
			
			color=new Vec4();
			color.x=(avgl); //verts[f.v1].getR()/31.0f;
			
			color.y=(avgl);// verts[f.v1].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v1].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			//normals
//			roomMesh.add(1.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v1.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v1.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);
			
			//XYZ
			pos=new Vec3();
			pos.x=((room_info.x+verts[f.v2].vertex.x)/-1024.0f);
			
			pos.y=(( - verts[f.v2].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v2].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color argb
			color=new Vec4();
			avgl=((verts[f.v2].getR() + verts[f.v2].getG() + verts[f.v2].getB()) / 3f) / 31f;
			color.x=(avgl);// verts[f.v2].getR()/31.0f;
			
			color.y=(avgl);// verts[f.v2].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v2].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			//normals
//			roomMesh.add(1.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			roomMesh.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v2.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v2.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);

			
		}
		
//		StringBuilder sb=new StringBuilder();
//		sb.append("mesh(");
//		sb.append(roomMesh.length);
//		sb.append(")");
//		for(int j=0;j<roomMesh.length;j++){
//			if(j%7==0)
//				sb.append("\n");
//			sb.append(roomMesh[j]);
//			sb.append(", ");
//		}
//		logger.log(sb.toString());
//
//		sb=new StringBuilder();
//		sb.append("indices(");
//		sb.append(roomIndices.length);
//		sb.append(")");
//		for(int j=0;j<roomIndices.length;j++){
//			sb.append(roomIndices[j]);
//			sb.append(", ");
//		}
//		logger.log(sb.toString());
		
        //first setup the mVertices correctly.
        //setup the shaders
        // Store the program object
        
        //now everything is setup and ready to draw.
    }
	
	
	public boolean hasVertices(){
		return true;
	}

	

	float[] mvpMatrix=new float[16];

	
    
	
}
