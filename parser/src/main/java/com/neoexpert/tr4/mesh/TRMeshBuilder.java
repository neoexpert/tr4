package com.neoexpert.tr4.mesh;
import com.neoexpert.mesh.*;

import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.mesh.*;
import com.neoexpert.tr4.level.room.*;
import java.nio.*;
import java.util.*;
import com.neoexpert.math.*;


public class TRMeshBuilder {
    private FloatBuffer vertices;
	private ShortBuffer indices;
	private boolean hasAdditiveBlending;
	private boolean hasVertices;

	private float mred;
	private float mgreen;
	private float mblue;
	private boolean canMark;

    public TRMeshBuilder(Mesh m, Level l) {
		super();

		Vertex center=m.centre;

		//room_info.yBottom=200;
		Vertex[] verts=m.vertices;
		MeshFace4[] rects=m.textRects;
		MeshFace3[] triangles=m.textTriangles;
		//rects=new Face4[1];

		int rectscount=1;
		int tricount=0;
		rectscount=rects.length;
		tricount=triangles.length;
		int meshSize=(rectscount*4+3*tricount)*(3+4+2);
		hasVertices=meshSize>0;
		MeshBuilder mb=new MeshBuilder();
		//float[] roomMesh=new float[meshSize];
		//short[] roomIndices=new short[tricount*3+ rectscount*6];
	
		//short fi=0;

		//int a=l.textures32.length;


		int twidth=(int)Math.sqrt(l.textures32.length)+1;
		for(int j=0;j< rectscount;j++){
			MeshFace4 f=rects[j];
			if(f.hasAlphaBlending())
				hasAdditiveBlending=true;
			int t=0;
			ObjectTexture ot=null;
			if (!f.isColored)
			{
				ot=l.levelData.objectTextures[f.getTexture()];
				t = ot.getTile();
			}
			int tx=t%twidth;
			int ty=t/twidth;

			//v0
			mb.addIndex((short)(mb.fi+0));

			mb.addIndex((short)(mb.fi+1));

			mb.addIndex((short)(mb.fi+2));

			mb.addIndex((short)(mb.fi+0));

			mb.addIndex((short)(mb.fi+2));

			mb.addIndex((short)(mb.fi+3));

			mb.fi+=4;


			
			Vec3 pos=new Vec3();
			pos.x=((verts[f.v0].x) / -1024.0f);

			pos.y=((verts[f.v0].y)/-1024.0f);

			pos.z=((verts[f.v0].z)/1024.0f);

			mb.addPosition(pos);
			
			//color
			Vec4 color=new Vec4();
			color.x=(1.0f);//verts[f.v0].getR()/32.0f;


			color.y=(1.0f);//verts[f.v0].getG()/32.0f;


			color.z=(1.0f);//verts[f.v0].getB()/32.0f;


			color.w=(1.0f);
			mb.addColor(color);
			
//			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			//f.t=3;
			Vec2 texcoord=new Vec2();
			if (!f.isColored)
			{
				texcoord.x=((tx * 256 + (ot.v0.xPixel)) / (twidth * 256.0f));
				texcoord.y=((ty * 256 + (ot.v0.yPixel)) / (twidth * 256.0f));
			}
			else{
				texcoord.x=(0);
				texcoord.y=(0);
			}
			mb.addTexCoord(texcoord);

			pos=new Vec3();
			pos.x=((verts[f.v1].x)/-1024.0f);
			
			pos.y=((verts[f.v1].y)/-1024.0f);
			
			pos.z=((verts[f.v1].z)/1024.0f);
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v1].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v1].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v1].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			if (!f.isColored)
			{
				texcoord.x=((tx * 256 + (ot.v1.xPixel)) / (twidth * 256.0f));
				

				texcoord.y=((ty * 256 + (ot.v1.yPixel)) / (twidth * 256.0f));
				
			}
			else{
				texcoord.x=(0);
				texcoord.y=(0);
			}
			mb.addTexCoord(texcoord);

			pos=new Vec3();
			pos.x=((verts[f.v2].x)/-1024.0f);
			
			pos.y=((verts[f.v2].y)/-1024.0f);
			
			pos.z=((verts[f.v2].z)/1024.0f);
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v2].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v2].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v2].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			if(!f.isColored){
				texcoord.x=((tx*256+(ot.v2.xPixel))/(twidth*256.0f));
				texcoord.y=((ty*256+(ot.v2.yPixel))/(twidth*256.0f));
			}
			else{
				texcoord.x=(0);
				texcoord.y=(0);
			}
			mb.addTexCoord(texcoord);

			pos=new Vec3();
			pos.x=((verts[f.v3].x)/-1024.0f);
			
			pos.y=((verts[f.v3].y)/-1024.0f);
			
			pos.z=((verts[f.v3].z)/1024.0f);
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v3].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v3].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v3].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			if(!f.isColored){
				texcoord.x=((tx*256+(ot.v3.xPixel))/(twidth*256.0f));
				texcoord.y=((ty*256+(ot.v3.yPixel))/(twidth*256.0f));
			}
			else{
				texcoord.x=(0);
				texcoord.y=(0);
			}
			mb.addTexCoord(texcoord);
		}


		//fi=0;
		for(int j=0;j< tricount;j++){

			MeshFace3 f=triangles[j];
			if(f.hasAlphaBlending())
				hasAdditiveBlending=true;
			int t=0;
			ObjectTexture ot=null;
			if (!f.isColored)
			{
				ot=l.levelData.objectTextures[f.getTexture()];
				t = ot.getTile();
			}
			int tx=t%twidth;
			int ty=t/twidth;
			//v0
			mb.addIndex((short)(mb.fi+0));

			mb.addIndex((short)(mb.fi+1));

			mb.addIndex((short)(mb.fi+2));

			mb.fi+=3;



			Vec3 pos=new Vec3();
			pos.x=((verts[f.v0].x) / -1024.0f);

			
			pos.y=((verts[f.v0].y)/-1024.0f);

			
			pos.z=((verts[f.v0].z)/1024.0f);

			mb.addPosition(pos);
			
			
			//color
			Vec4 color=new Vec4();
			color.x=(1.0f);//verts[f.v0].getR()/32.0f;

			color.y=(1.0f);//verts[f.v0].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v0].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			
//			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			Vec2 textcoord=new Vec2();
			if (!f.isColored)
			{
				textcoord.x=((tx * 256 + (ot.v0.xPixel)) / (twidth * 256.0f));
				textcoord.y=((ty*256+(ot.v0.yPixel))/(twidth*256.0f));
			}
			else{
				textcoord.x=(0);
				textcoord.y=(0);
			}
			mb.addTexCoord(textcoord);

			pos=new Vec3();
			pos.x=((verts[f.v1].x)/-1024.0f);
			
			pos.y=((verts[f.v1].y)/-1024.0f);
			
			pos.z=((verts[f.v1].z)/1024.0f);
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v1].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v1].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v1].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			textcoord=new Vec2();
			if(!f.isColored){
				textcoord.x=((tx*256+(ot.v1.xPixel))/(twidth*256.0f));
			
				textcoord.y=((ty*256+(ot.v1.yPixel))/(twidth*256.0f));
			}
			else{
				textcoord.x=(0);
				textcoord.y=(0);
			}
			mb.addTexCoord(textcoord);

			pos=new Vec3();
			pos.x=((verts[f.v2].x)/-1024.0f);
			
			pos.y=((verts[f.v2].y)/-1024.0f);
			
			pos.z=((verts[f.v2].z)/1024.0f);
			
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v2].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v2].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v2].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			textcoord=new Vec2();
			if(!f.isColored){
				textcoord.x=((tx*256+(ot.v2.xPixel))/(twidth*256.0f));
				textcoord.y=((ty*256+(ot.v2.yPixel))/(twidth*256.0f));
			}
			else{
				textcoord.x=(0);
				textcoord.y=(0);
			}
			mb.addTexCoord(textcoord);
		}


		vertices=mb.genVertices();
        


		indices= mb.genIndices();
		
    }
	List<TRMeshBuilder> children=new ArrayList<>();

	public TRMeshBuilder getChild(int index)
	{
		return children.get(index);
	}
	public void addChild(TRMeshBuilder m)
	{
		//Matrix.multiplyMM(m.mModelMatrix, 0, mModelMatrix,0, m.mModelMatrix, 0);
		children.add(m);
	}

	public boolean hasAddidiveBlending()
	{
		return hasAdditiveBlending;
	}

	public ShortBuffer getIndices() {
		return indices;
	}
	public FloatBuffer getVertices(){
		return vertices;
	}
}
