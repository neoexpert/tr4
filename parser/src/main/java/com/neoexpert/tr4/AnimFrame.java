package com.neoexpert.tr4;

import com.neoexpert.tr4.level.*;
import java.io.*;
import com.neoexpert.math.*;

public class AnimFrame{
	public BoundingBox bounding_box;
	public short offsetX;                                         
	public short offsetY;
	public short offsetZ;
	int[] angles;
	public AnimFrame(EndianDataInputStream dis, int size) throws IOException{
		bounding_box=new BoundingBox(dis);
		offsetX= (short)((dis.read() & 0xff) | ((dis.read() & 0xff)<<8));
		offsetY= (short)((dis.read() & 0xff) | ((dis.read() & 0xff)<<8));
		offsetZ= (short)((dis.read() & 0xff) | ((dis.read() & 0xff)<<8));
		int pos=12+6;
		int angles=(size-pos)/2;
		this.angles=new int[angles];
		for(int i=0;i<angles;++i){
			this.angles[i]=dis.readShort()&0xFFFF;
		}
	}
	public static final float PI2=(float)(Math.PI*2.0);
	public Vec3 getAngle(int joint){
		int index=0;
		for (int i = 0; i < joint; i++) {
			if ((angles[index++] & 0xC000)==0){
				index++;
			}
		}
		int a = angles[index++];
		float rot;                                                    
		//if (((version & VER_VERSION) >= VER_TR4)) {
			rot = (float)(a & 0x0FFF) * (PI2 / 4096.0f);
		//} else {
			rot = (float)(a & 0x03FF) * (PI2 / 1024.0f);
		//} 
		switch (a & 0xC000) {
			case 0x4000 : return new Vec3(rot, 0, 0);
			case 0x8000 : return new Vec3(0, rot, 0);
			case 0xC000 : return new Vec3(0, 0, rot);
			default     : return unpack(a, angles[index++]);
		}
	}
	private static final float ANGLE_SCALE = (PI2 / 1024.0f);
	private Vec3 unpack(int a, int b){
		 return new Vec3((float)((a & 0x3FF0) >> 4), (float)( ((a & 0x000F) << 6) | ((b & 0xFC00) >> 10)), (float)(b & 0x03FF)).multiply(ANGLE_SCALE);
	}
}
