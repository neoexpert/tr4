package com.neoexpert.tr4.rendering.texture;

public class TRTexture {
    public interface ABitmap{
        void setPixel(int x, int y, int c);
        void createBitmap(int w, int h);
    }

    public static void loadTRTexture(com.neoexpert.tr4.level.Texture[] t, ABitmap bmp)
    {
        int w=(int)Math.sqrt(t.length)+1;
        int twidth=w;
        bmp.createBitmap(256*w, 256*w); // this creates a MUTABLE bitmap
        for (int iy=0;iy < twidth;iy++)
        {
            for (int ix=0;ix < twidth;ix++)
            {
                int ti=iy*twidth+ix;
                if(ti>=t.length)
                    break;
                com.neoexpert.tr4.level.Texture tt=t[ti];
                for (int x=0;x < 256;x++)
                    for (int y=0;y < 256;y++)
                    {
                        int c=tt.pixels[y][x];
                        bmp.setPixel(ix * 256 + x,  iy*256+y, c);
                    }
            }
        }

    }
}
