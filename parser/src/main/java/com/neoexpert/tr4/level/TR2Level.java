package com.neoexpert.tr4.level;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.level.palette.*;

import java.io.*;

import com.neoexpert.tr4.level.mesh.*;
import com.neoexpert.tr4.level.animation.*;


/**
 * Hello world!
 *
 */
public class TR2Level extends Level{
	TRColour[] palette=new TRColour[256];
	TRColour4[] palette16=new TRColour4[256];
	private int rgbaToArgb(int x){
    return
    // Source is in format: 0xAARRGGBB
        ((x & 0xFF000000) >> 24) | //______AA
        ((x & 0x00FF0000) >>  8) | //____RR__
        ((x & 0x0000FF00) <<  8) | //__GG____
        ((x & 0x000000FF) << 24);  //BB______
    // Return value is in format:  0xBBGGRRAA
	}
	protected TR2Level(EndianDataInputStream din)throws IOException
	{
		super(TR2);
		for(int i=0;i<palette.length;i++)
			palette[i]=new TRColour(din);
		for(int i=0;i<palette16.length;i++)
			palette16[i]=new TRColour4(din);


		int numImages=din.readInt();
		System.out.println("numImages: "+ (int)numImages);
		TRImage8[] images8=new TRImage8[numImages];
		for(int i=0;i<numImages;i++)
			images8[i]=new TRImage8(din);

		TRImage16[] images16=new TRImage16[numImages];
		for(int i=0;i<numImages;i++)
			images16[i]=new TRImage16(din);
		textures32=new Texture[numImages];
		for(int i=0;i<numImages;i++){
			Texture t=new Texture(256,256);
			textures32[i]=t;
			TRImage16 img8=images16[i];
			int j=0;
			for(int x=0;x<256;x++){
				for(int y=0;y<256;y++){
					int c=img8.pixels[x][y];

					if(((c >> 15) & 1)==1)
							c = (((c & 0x00007c00) >> 7)<<16) | (((c & 0x000003e0) >> 2) << 8) | (((c & 0x0000001f) << 3)) | 0xff000000;
						else
							c = 0x00000000;
					t.pixels[x][y]=c;
				}
			}
		}
		int unused=din.readInt();
		int numRooms=din.readShort();
		System.out.println("numRooms: "+ (int)numRooms);
		levelData=new LevelData(this);
		levelData.rooms=new Room[numRooms];
		for(int i=0;i<numRooms;i++){
			levelData.rooms[i]=new Room(din,this);
		}
		int numFloorData= din.readInt();
		levelData.floorData=new short[numFloorData];
		System.out.println("numFloorData: "+numFloorData);
		for(int i=0;i<numFloorData;i++){
			levelData.floorData[i]=din.readShort();
		}
		int numMeshData= din.readInt();
		System.out.println("numMeshData: "+numMeshData);
		byte[] meshData=new byte[numMeshData*2];

		din.readFully(meshData);
		int numMeshPointers= din.readInt();
		levelData.meshPointers=new int[numMeshPointers];
		//for(int i=0;i<numMeshPointers;i++)

		System.out.println("numMeshPointers: "+numMeshPointers);

		for(int i=0;i<numMeshPointers;i++){
			int pointer=din.readInt();
			levelData.meshPointers[i]=pointer;
			ByteArrayInputStream bais = new ByteArrayInputStream(meshData);

			DataInputStream dismeshes=new DataInputStream(bais);
			EndianDataInputStream dinm = new EndianDataInputStream(dismeshes);
			dinm.skipBytes(pointer);
			levelData.meshes.put(pointer,new Mesh(dinm,version));


		}
		int numAnimations= din.readInt();
		System.out.println("numAnimations: "+numAnimations);
		levelData.animations=new TRAnimation[numAnimations];
		for(int i=0;i<numAnimations;i++){
			levelData.animations[i]=new TRAnimation(din,version);
		}

		int numStateChanges= din.readInt();
		levelData.stateChanges=new StateChange[numStateChanges];
		System.out.println("numStateChanges: "+numStateChanges);
		for(int i=0;i<numStateChanges;i++){
			levelData.stateChanges[i]=new StateChange(din);	
		}
		int numAnimDispatches= din.readInt();
		levelData.animDispatches=new AnimDispatch[numAnimDispatches];
		System.out.println("numAnimDispatches: "+numAnimDispatches);
		for(int i=0;i<numAnimDispatches;i++){
			levelData.animDispatches[i]=new AnimDispatch(din);	
		}
		int numAnimCommands= din.readInt();
		levelData.animCommands=new char[numAnimCommands];
		System.out.println("numAnimCommands: "+numAnimCommands);
		for(int i=0;i<numAnimCommands;i++){
			levelData.animCommands[i]=din.readChar();	
		}

		int numMeshTrees = din.readInt();
		levelData.meshTrees=new int[numMeshTrees];
		System.out.println("numMeshTrees: "+numMeshTrees);
		for(int i=0;i<numMeshTrees;i++){
			levelData.meshTrees[i]=din.readInt();
		}

		int numFrames = din.readInt();
		levelData.frames=new byte[numFrames*2];
		System.out.println("numFrames: "+numFrames);
		din.readFully(levelData.frames);

		int numMoveables = din.readInt();
		levelData.moveables=new Model[numMoveables];
		System.out.println("numMoveables: "+numMoveables);
		for(int i=0;i<numMoveables;i++){
			levelData.moveables[i]=new Model(din);
		}



		int numStaticMeshes = din.readInt();
		levelData.staticMeshes=new StaticMesh[numStaticMeshes];
		System.out.println("numStaticMeshes: "+numStaticMeshes);
		for(int i=0;i<numStaticMeshes;i++){
			levelData.staticMeshes[i]=new StaticMesh(din);
		}

		int numObjectTextures=din.readInt();
		System.out.println("numObjectTextures: "+numObjectTextures);
		levelData.objectTextures=new ObjectTexture[numObjectTextures];
		for(int i=0;i<numObjectTextures;i++){
			levelData.objectTextures[i]=new ObjectTexture(din,version);
		}
		int numSpriteTextures = din.readInt();
		levelData.spriteTextures=new SpriteTexture[numSpriteTextures];
		System.out.println("numSpriteTextures: "+numSpriteTextures);
		for(int i=0;i<numSpriteTextures;i++){
			levelData.spriteTextures[i]=new SpriteTexture(din);
		}

		int numSpriteSequences = din.readInt();
		levelData.spriteSequences=new SpriteSequence[numSpriteSequences];
		System.out.println("numSpriteSequences: "+numSpriteSequences);
		for(int i=0;i<numSpriteSequences;i++){
			levelData.spriteSequences[i]=new SpriteSequence(din);
		}

		int numCameras = din.readInt();
		levelData.cameras=new Camera[numCameras];
		System.out.println("numCameras: "+numCameras);
		for(int i=0;i<numCameras;i++){
			levelData.cameras[i]=new Camera(din);
		}

		int numSoundSources = din.readInt();
		levelData.soundSources=new SoundSource[numSoundSources];
		System.out.println("numSoundSources: "+numSoundSources);
		for(int i=0;i<numSoundSources;i++){
			levelData.soundSources[i]=new SoundSource(din);
		}

		int numBoxes = din.readInt();
		System.out.println("numBoxes: "+numBoxes);
		levelData.boxes=new Box[numBoxes];
		for(int i=0;i<numBoxes;i++){
			levelData.boxes[i]=new Box(din);
		}

		int numOverlaps = din.readInt();
		levelData.overlaps=new char[numOverlaps];
		System.out.println("numOverlaps: "+numOverlaps);
		for(int i=0;i<numOverlaps;i++){
			levelData.overlaps[i]=din.readChar();
		}

		int numZones=numBoxes*10;
		levelData.zones=new char[numZones];
		for(int i=0;i<numZones;i++){
			levelData.zones[i]=din.readChar();
		}
		int numAnimatedTextures = din.readInt();
		levelData.animatedTextures=new int[numAnimatedTextures];
		System.out.println("numAnimatedTextures: "+numAnimatedTextures);
		for(int i=0;i<numAnimatedTextures;i++){
			levelData.animatedTextures[i]=din.readShort();
		}


		int numItems=din.readInt();
		System.out.println("numItems: "+numItems);
		levelData.items=new Entity[numItems];
		for(int i=0;i<numItems;i++){
			levelData.items[i]=new Entity(din);
			if(levelData.items[i].typeID==0)
			{
				if(levelData.lara!=null)
					throw new RuntimeException("lara was already found");
				levelData.lara=levelData.items[i];
				System.out.println("lara found: "+levelData.lara);
			}
		}
		//uint8_t LightMap[32 * 256]; // light map (8192 bytes)
		int lightMap[][]=new int[32][256];
		for(int x=0;x<32;x++)
			for(int y=0;y<256;y++)
				lightMap[x][y]=din.readByte()&0xFF;



		int numCinematicFrames=din.readShort();
		System.out.println("numCinematicFrames: "+numCinematicFrames);
		CinematicFrame[] cinematicFrames=new CinematicFrame[numCinematicFrames];
		for(int i=0;i<numCinematicFrames;i++){
			cinematicFrames[i]=new CinematicFrame(din);

		}
		int numDemoData=din.readShort();
		System.out.println("numDemoData: "+numDemoData);
		levelData.demoData=new int[numDemoData];
		for(int i=0;i<numDemoData;i++){
			levelData.demoData[i]=din.readByte();
		}
		levelData.soundMap=new char[370];
		for(int i=0;i<370;i++){
			levelData.soundMap[i]=din.readChar();
		}

		int numSoundDetails=din.readInt();
		System.out.println("numSoundDetails: "+numSoundDetails);
		levelData.soundDetails=new SoundDetail[numSoundDetails];
		for(int i=0;i<numSoundDetails;i++){
			levelData.soundDetails[i]=new SoundDetail(din);
		}

		int numSampleIndices=din.readInt();
		System.out.println("numSampleIndices: "+numSampleIndices);
		levelData.sampleIndices=new int[numSampleIndices];
		for(int i=0;i<numSampleIndices;i++){
			levelData.sampleIndices[i]=din.readInt();
		}
		
	}
	static int little2big(int i) {
		return (i&0xff)<<24 | (i&0xff00)<<8 | (i&0xff0000)>>8 | (i>>24)&0xff;
	}	
	public static void writeToFile(String name,byte[] bytes)throws IOException{
		FileOutputStream stream = new FileOutputStream(name);
		try {
			stream.write(bytes);
		} finally {
			stream.close();
		}	
	}
	public TRLight createLight(){
		return new TR2Light();
	}
}
/*
uint8_t Textile32_Compressed[Textile32_CompSize]; // zlib-compressed 32-bit textures chunk (Textile32_CompSize bytes)
{
    tr4_textile32 Textile32[NumRoomTextiles + NumObjTextiles + NumBumpTextiles];
}
uint32_t Textile16_UncompSize; // uncompressed size (in bytes) of the 16-bit textures chunk (4 bytes)
uint32_t Textile16_CompSize; // compressed size (in bytes) of the 16-bit textures chunk (4 bytes)
uint8_t Textile16_Compressed[Textile32_CompSize]; // zlib-compressed 16-bit textures chunk (Textile16_CompSize bytes)
{
    tr_textile16 Textile16[NumRoomTextiles + NumObjTextiles + NumBumpTextiles];
}
uint32_t Textile32Misc_UncompSize; // uncompressed size (in bytes) of the 32-bit misc textures chunk (4 bytes), should always be 524288
uint32_t Textile32Misc_CompSize; // compressed size (in bytes) of the 32-bit misc textures chunk (4 bytes)
uint8_t Textile32Misc_Compressed[Textile32Misc_CompSize]; // zlib-compressed 32-bit misc textures chunk (Textile32Misc_CompSize bytes)
{
    tr4_textile32 Textile32Misc[2];
}
uint32_t LevelData_UncompSize; // uncompressed size (in bytes) of the level data chunk (4 bytes)
uint32_t LevelData_CompSize; // compressed size (in bytes) of the level data chunk (4 bytes)
uint8_t LevelData_Compressed[LevelData_CompSize]; // zlib-compressed level data chunk (LevelData_CompSize bytes)
{
    uint32_t Unused; // 32-bit unused value, always 0 (4 bytes)
    uint16_t NumRooms; // number of rooms (2 bytes)
    tr4_room Rooms[NumRooms]; // room list (variable length)
    uint32_t NumFloorData; // number of floor data uint16_t's to follow (4 bytes)
    uint16_t FloorData[NumFloorData]; // floor data (NumFloorData * 2 bytes)
    uint32_t NumMeshData; // number of uint16_t's of mesh data to follow (=Meshes[]) (4 bytes)
    tr4_mesh Meshes[NumMeshPointers]; // note that NumMeshPointers comes AFTER Meshes[]
    uint32_t NumMeshPointers; // number of mesh pointers to follow (4 bytes)
    uint32_t MeshPointers[NumMeshPointers]; // mesh pointer list (NumMeshPointers * 4 bytes)
    uint32_t NumAnimations; // number of animations to follow (4 bytes)
    tr4_animation Animations[NumAnimations]; // animation list (NumAnimations * 40 bytes)
    uint32_t NumStateChanges; // number of state changes to follow (4 bytes)
    tr_state_change StateChanges[NumStateChanges]; // state-change list (NumStructures * 6 bytes)
    uint32_t NumAnimDispatches; // number of animation dispatches to follow (4 bytes)
    tr_anim_dispatch AnimDispatches[NumAnimDispatches]; // animation-dispatch list list (NumAnimDispatches * 8 bytes)
    uint32_t NumAnimCommands; // number of animation commands to follow (4 bytes)
    tr_anim_command AnimCommands[NumAnimCommands]; // animation-command list (NumAnimCommands * 2 bytes)
    uint32_t NumMeshTrees; // number of MeshTrees to follow (4 bytes)
    tr_meshtree_node MeshTrees[NumMeshTrees]; // MeshTree list (NumMeshTrees * 4 bytes)
    uint32_t NumFrames; // number of words of frame data to follow (4 bytes)
    uint16_t Frames[NumFrames]; // frame data (NumFrames * 2 bytes)
    uint32_t NumMoveables; // number of moveables to follow (4 bytes)
    tr_model Moveables[NumMoveables]; // moveable list (NumMoveables * 18 bytes)
    uint32_t NumStaticMeshes; // number of StaticMesh data records to follow (4 bytes)
    tr_staticmesh StaticMeshes[NumStaticMeshes]; // StaticMesh data (NumStaticMesh * 32 bytes)
    uint8_t SPR[3]; // S P R (0x53, 0x50, 0x52)
    uint32_t NumSpriteTextures; // number of sprite textures to follow (4 bytes)
    tr_sprite_texture SpriteTextures[NumSpriteTextures]; // sprite texture list (NumSpriteTextures * 16 bytes)
    uint32_t NumSpriteSequences; // number of sprite sequences records to follow (4 bytes)
    tr_sprite_sequence SpriteSequences[NumSpriteSequences]; // sprite sequence data (NumSpriteSequences * 8 bytes)
    uint32_t NumCameras; // number of camera data records to follow (4 bytes)
    tr_camera Cameras[NumCameras]; // camera data (NumCameras * 16 bytes)
    uint32_t NumFlybyCameras; // number of flyby camera data records to follow (4 bytes)
    tr4_flyby_camera FlybyCameras[NumFlybyCameras]; // flyby camera data (NumFlybyCameras * 40 bytes)
    uint32_t NumSoundSources; // number of sound source data records to follow (4 bytes)
    tr_sound_source SoundSources[NumSoundSources]; // sound source data (NumSoundSources * 16 bytes)
    uint32_t NumBoxes; // number of box data records to follow (4 bytes)
    tr2_box Boxes[NumBoxes]; // box data (NumBoxes * 8 bytes)
    uint32_t NumOverlaps; // number of overlap records to follow (4 bytes)
    uint16_t Overlaps[NumOverlaps]; // overlap data (NumOverlaps * 2 bytes)
    int16_t Zones[10*NumBoxes]; // zone data (NumBoxes * 20 bytes)
    uint32_t NumAnimatedTextures; // number of animated texture records to follow (4 bytes)
    uint16_t AnimatedTextures[NumAnimatedTextures]; // animated texture data (NumAnimatedTextures * 2 bytes)
    uint8_t AnimatedTexturesUVCount;
    uint32_t NumObjectTextures; // number of object textures to follow (4 bytes) (after AnimatedTextures in TR3)
    tr4_object_texture ObjectTextures[NumObjectTextures]; // object texture list (NumObjectTextures * 38 bytes)
    uint32_t NumItems; // number of items to follow (4 bytes)
    tr4_entity Items[NumItems]; // item list (NumItems * 24 bytes)
    uint32_t NumAIObjects; // number of AI objects to follow (4 bytes)
    tr4_ai_object AIObjects[NumAIObjects]; // AI objects list (NumAIObjects * 24 bytes)
    uint16_t NumDemoData; // number of demo data records to follow (2 bytes)
    uint8_t DemoData[NumDemoData]; // demo data (NumDemoData bytes)
    int16_t SoundMap[370]; // sound map (740 bytes)
    uint32_t NumSoundDetails; // number of sound-detail records to follow (4 bytes)
    tr3_sound_details SoundDetails[NumSoundDetails]; // sound-detail list (NumSoundDetails * 8 bytes)
    uint32_t NumSampleIndices; // number of sample indices to follow (4 bytes)  +
    uint32_t SampleIndices[NumSampleIndices]; // sample indices (NumSampleIndices * 4 bytes)
}
uint32_t NumSamples; // number of sound samples (4 bytes)
tr4_sample Samples[NumSamples]; // sound samples (this is the last part, so you can simply read until EOF)
*/

