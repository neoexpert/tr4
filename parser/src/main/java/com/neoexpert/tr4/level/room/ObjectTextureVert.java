package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class ObjectTextureVert{
	public int xCoord;
	public int xPixel;
	public int	yCoord;
	public int yPixel;
	public ObjectTextureVert(EndianDataInputStream din)throws IOException{
		xCoord=(byte)din.read()& 0xff;
		xPixel=(byte)din.read()& 0xff;
		yCoord=(byte)din.read()& 0xff;
		yPixel=(byte)din.read()& 0xff;
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("x: "+(xPixel));
		sb.append("\n");
		sb.append("xc: "+(xCoord));
		sb.append("\n");
		sb.append("y: "+(yPixel));
		sb.append("\n");
		sb.append("yc: "+(yCoord));
		return sb.toString();
	}
}
/*
struct tr_object_texture_vert // 4 bytes
{
    uint8_t Xcoordinate; // 1 if Xpixel is the low value, 255 if Xpixel is the high value in the object texture
    uint8_t Xpixel;
    uint8_t Ycoordinate; // 1 if Ypixel is the low value, 255 if Ypixel is the high value in the object texture
    uint8_t Ypixel;
};
 * */
