package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class SpriteTexture{
	public SpriteTexture(EndianDataInputStream din)throws IOException{
		din.readChar();
		din.read();
		din.read();
		din.readChar();
		din.readChar();
		din.readChar();
		din.readChar();
		din.readChar();
		din.readChar();
	}
}
/*
struct tr_sprite_texture   // 16 bytes
{
    uint16_t Tile;
     uint8_t x;
     uint8_t y;
    uint16_t Width;        // (ActualWidth  * 256) + 255
    uint16_t Height;       // (ActualHeight * 256) + 255
     int16_t LeftSide;
     int16_t TopSide;
     int16_t RightSide;
     int16_t BottomSide;
};
 * */
