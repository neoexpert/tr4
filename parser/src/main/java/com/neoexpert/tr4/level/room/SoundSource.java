package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class SoundSource{
	public SoundSource(EndianDataInputStream din)throws IOException{
		din.readInt();
		din.readInt();
		din.readInt();
		din.readChar();
		din.readChar();
	}
}
/*
struct tr_sound_source // 16 bytes
{
     int32_t x;         // absolute X position of sound source (world coordinates)
     int32_t y;         // absolute Y position of sound source (world coordinates)
     int32_t z;         // absolute Z position of sound source (world coordinates)
    uint16_t SoundID;   // internal sound index
    uint16_t Flags;     // 0x40, 0x80, or 0xC0
};
 * */
