package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.palette.*;

import com.neoexpert.*;
import java.io.*;

public abstract class TRLight{
	public static final int SUN=0;
	public static final int LIGHT=1;
	public static final int SPOT=2;
	public static final int SHADOW=3;
	public static final int FOGBULB=4;
	public Color color;
	public int x,y,z;
	public float dx,dy,dz;
	public int type;
	public abstract void read(EndianDataInputStream din)throws IOException;
}
