package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class Camera{
	public Camera(EndianDataInputStream din)throws IOException{
		din.readInt();
		din.readInt();
		din.readInt();
		din.readShort();
		din.readShort();
	}
}
/*
struct tr_camera // 16 bytes
{
    int32_t x;
    int32_t y;
    int32_t z;
    int16_t Room;
   uint16_t Flag;
};
 * */
