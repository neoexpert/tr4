package com.neoexpert.tr4.level;

import com.neoexpert.*;
import java.io.*;

public class Box{
	public Box(EndianDataInputStream din)throws IOException{
		din.read();
		din.read();
		din.read();
		din.read();
		din.readChar();
		din.readChar();
	}
}
/*
struct tr2_box   // 8 bytes
{
    uint8_t Zmin;          // Horizontal dimensions in sectors
    uint8_t Zmax;
    uint8_t Xmin;
    uint8_t Xmax;
    int16_t TrueFloor;     // Height value in global units
    int16_t OverlapIndex;  // Index into Overlaps[] (same as tr_box?)
};
*/
