package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class ObjectTexture{
	public ObjectTextureVert v0;
	public ObjectTextureVert v1;
	public ObjectTextureVert v2;
	public ObjectTextureVert v3;
	public char attr;
	public char tileAndFlag;
	public char newFlags;
    public int originalU;
    public int originalV;
    public int width;     // Actually width-1
    public int height;    // Actually height-1
	public ObjectTexture(EndianDataInputStream din,int version)throws IOException{
		attr=din.readChar();
		tileAndFlag = (char)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		if(version==Level.TR4||version==Level.TR5)
			newFlags=din.readChar();
		v0=new ObjectTextureVert(din);
		v1=new ObjectTextureVert(din);
		v2=new ObjectTextureVert(din);
		v3=new ObjectTextureVert(din);

		if(version==Level.TR4||version==Level.TR5){
			originalU=din.readInt();
			originalV=din.readInt();
			width=din.readInt();
			height=din.readInt();
		}
	}
	public int getTile(){
		return tileAndFlag  & ~(1 << 15);
	}
	public boolean isTriangle(){
		return (tileAndFlag & 1 << 15) != 0;
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("tile: "+getTile());
		sb.append("\n");
		sb.append("v0: "+v0);
		sb.append("\n");
		sb.append("v1: "+v1);
		sb.append("\n");
		sb.append("v2: "+v2);
		sb.append("\n");
		sb.append("v3: "+v3);
		return sb.toString();
	}
}
/*
struct tr4_object_texture // 38 bytes
{
    uint16_t Attribute;
    uint16_t TileAndFlag;
    uint16_t NewFlags;

    tr_object_texture_vert Vertices[4]; // The four corners of the texture

    uint32_t OriginalU;
    uint32_t OriginalV;
    uint32_t Width;     // Actually width-1
    uint32_t Height;    // Actually height-1
};
 * */
