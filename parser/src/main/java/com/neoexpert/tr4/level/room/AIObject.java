package com.neoexpert.tr4.level.room;

import com.neoexpert.tr4.level.*;
import java.io.*;

public class AIObject{
	public AIObject(EndianDataInputStream din)throws IOException{
		din.readChar();
		din.readChar();
		din.readInt();
		din.readInt();
		din.readInt();
		din.readChar();
		din.readChar();
		din.readInt();
	}
}
/*
struct tr4_ai_object   // 24 bytes
{
    uint16_t TypeID     // Object type ID (same meaning as with tr4_entity)
    uint16_t Room;      // Room where AI object is placed
     int32_t x, y, z;   // Coordinates
     int16_t OCB;       // Same meaning as with tr4_entity
    uint16_t Flags;     // Activation mask, bitwise-shifted left by 1
     int32_t Angle;
};
 * */
