package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import com.neoexpert.math.*;
import java.io.*;

public class Portal{
	public final int toRoom;
	public final Vertex normal;
	public final Vertex v0,v1,v2,v3;
	public final Vec3 center;
	public final float radius;
	private Level level;
	public Portal(EndianDataInputStream din,RoomInfo ri, Level level)throws IOException{
		this.level=level;
		toRoom = (din.read() & 0xff) | ((din.read() & 0xff)<<8);

		normal=new Vertex(din);
		
		v0=new Vertex(din);

		v1=new Vertex(din);
		
		v2=new Vertex(din);

		v3=new Vertex(din);
		float avgX=
			(v0.x+v1.x+v2.x+v3.x)/4f;
		float avgY=
			(v0.y+v1.y+v2.y+v3.y)/4f;
		float avgZ=
			(v0.z+v1.z+v2.z+v3.z)/4f;
		Vec3 center=new Vec3(avgX/-1024f,avgY/-1024f,avgZ/1024f);
		radius=
		center.subtract(new Vec3(v0.x/-1024f,v0.y/-1024f,v0.z/1024f)).length();
		this.center=center.add(new Vec3(ri.x/-1024f,0,ri.z/1024f));
	}

	public Room getRoom(){
		return level.levelData.rooms[toRoom];
	}
}
/*
struct tr_room_portal  // 32 bytes
{
    uint16_t  AdjoiningRoom; // Which room this portal leads to
    tr_vertex Normal;
    tr_vertex Vertices[4];
};
 * */
