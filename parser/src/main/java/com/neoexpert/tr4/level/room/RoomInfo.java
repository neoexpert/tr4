package com.neoexpert.tr4.level.room;

import com.neoexpert.*;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.*;
import java.io.*;
import java.nio.ByteOrder;

public class RoomInfo{
	public RoomInfo(EndianDataInputStream din, int version)throws IOException{
		x=din.readInt();
		if(version==Level.TR5)
			y=din.readInt();
		z=din.readInt();
		yBottom=din.readInt();
		yTop=din.readInt();
	}
	public int x;             // X-offset of room (world coordinates)
	public int y;             // Y-offset of room (world coordinates)
	public int z;             // Z-offset of room (world coordinates)
	public int yBottom;
	public int yTop;
	@Override
	public String toString(){
		return "x: "+(x)+" z: "+(z)+" yBottom: "+yBottom+" yTop: "+yTop;
	}
}
