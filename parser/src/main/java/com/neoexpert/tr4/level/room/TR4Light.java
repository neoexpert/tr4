package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class TR4Light extends TRLight{
	public byte intensity;
	public float in;
	public float out;
	public float length;
	public float cutOff;
	public void read(EndianDataInputStream din)throws IOException{
		x=din.readInt();
		y=din.readInt();
		z=din.readInt();
		color=new Color(din);
		type=din.readByte()&0xFF;
		System.out.println("light_redundant_byte: "+din.read());
		intensity=din.readByte();

		in=din.readFloat();
		out=din.readFloat();
		length=din.readFloat();
		cutOff=din.readFloat();

		dx=din.readFloat();
		dy=din.readFloat();
		dz=din.readFloat();
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(");
		sb.append(x);
		sb.append(",");
		sb.append(y);
		sb.append(",");
		sb.append(z);
		sb.append(") ");
		sb.append(" type: ");
		sb.append(type);
		sb.append(" intensity: ");
		sb.append(intensity);
		return sb.toString();
	}
}
/*
struct tr4_room_light   // 46 bytes
{
        int32_t x, y, z;       // Position of light, in world coordinates
      tr_colour Colour;        // Colour of the light

        uint8_t LightType;
        uint8_t Unknown;       // Always 0xFF?
        uint8_t Intensity;

          float In;            // Also called hotspot in TRLE manual
          float Out;           // Also called falloff in TRLE manual
          float Length;
          float CutOff;

          float dx, dy, dz;    // Direction - used only by sun and spot lights
};
 * */
