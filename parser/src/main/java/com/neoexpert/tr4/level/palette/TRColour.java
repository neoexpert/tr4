package com.neoexpert.tr4.level.palette;

import com.neoexpert.*;
import com.neoexpert.tr4.level.*;
import java.io.*;

public class TRColour{
	public int r,g,b;
	public TRColour(EndianDataInputStream din)throws IOException{
		r=din.readByte()&0xFF;
		g=din.readByte()&0xFF;
;
		b=din.readByte()&0xFF;
;

	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(");
		sb.append(r);
		sb.append(",");
		sb.append(g);
		sb.append(",");
		sb.append(b);
		sb.append(")");
		return sb.toString();
	}
}
/*
struct tr_vertex   // 6 bytes
{
    int16_t x;
    int16_t y;
    int16_t z;
};
 * */
