package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import java.io.*;
import java.nio.*;
import com.neoexpert.*;



/**
 * Hello world!
 *
 */
public class TR5Room
{
	public RoomInfo room_info;
	byte[] datawords;
	int numZsectors;
	int numXsectors;
	int numLights;
	public TRLight[] lights;
	int numStaticMeshes;
	public RoomStaticMesh[] staticMeshes;
	public Portal[] portals;
	public int alternateRoom;
	int flags;

	int waterScheme;
	int reverbInfo;
	int alternate_group;

	public TR5Room(EndianDataInputStream din,Level level)throws IOException
	{
		int xela=din.readInt();
		System.out.println("ALEX:"+Integer.toHexString(xela));
		int roomDataSize=din.readInt();
		System.out.println("roomDataSize:"+roomDataSize);
		//din.skipBytes(roomDataSize);
		int separator=din.readInt();
		System.out.println("separator:"+separator);
		
		int endSDOffset=din.readInt();
		int startSDOffset=din.readInt();

		//separator
		din.readInt();

		int endPortalOffset=din.readInt();
		room_info=new RoomInfo(din,level.version);

		System.out.println(room_info);

		numZsectors = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numZsectors:"+numZsectors);
		numXsectors = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numXsectors:"+numXsectors);

		//RoomColor:
		din.readInt();

		numLights = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numLights:"+numLights);
		numStaticMeshes = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numStaticMeshes:"+numStaticMeshes);

		reverbInfo=din.read();
		alternate_group=din.read();
		waterScheme=din.readShort();

		//filler
		if(din.readInt()!=din.readInt())
			throw new IOException("filler were not equal");
		//separator
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		//separator
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		//filler
		din.readInt();

		alternateRoom=din.readShort();
		flags=din.readShort();
		//unknown
		din.readInt();
		if(din.readInt()!=0)
			throw new IOException("wrong unknown separator");
		if(din.readInt()!=0)
			throw new IOException("wrong unknown separator");
		//separator
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");

		din.readShort();
		din.readShort();

		float x=din.readFloat();
		float y=din.readFloat();
		float z=din.readFloat();

		//separators
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		//null room indicator
		boolean null_room=din.readInt()!=0;
		if(null_room)
			System.out.println("NULL ROOM");
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");

		int numRoomTriangles=din.readInt();
		System.out.println("numRoomTriangles:"+numRoomTriangles);
		int numRoomRectangles=din.readInt();
		System.out.println("numRoomRectangles:"+numRoomRectangles);

		//separator
		if(din.readInt()!=0)
			throw new IOException("wrong zero separator");

		int lightDataSize=din.readInt();
		int numLights2=din.readInt();
		//unknown
		din.readInt();

		int yTop=din.readInt();
		int yBottom=din.readInt();
		int numLayers=din.readInt();
		int layerOffset=din.readInt();
		int verticesOffset=din.readInt();
		int polyOffset=din.readInt();
		int polyOffset2=din.readInt();
		if(polyOffset!=polyOffset2)
			throw new IOException("polyOffset!=polyOffset2");
		int numVertices=din.readInt();
		if ((numVertices % 28) != 0)
			throw new IOException("wrong numVertices value");
		//separators
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		if(din.readInt()!=separator)
			throw new IOException("wrong.separator");
		lights=new TRLight[numLights];
		System.out.println("numLights:"+numLights);
		for(int i=0;i<numLights;i++){
			TRLight light=level.createLight();
			light.read(din);
			lights[i]=light;
			System.out.println("Light "+i+lights[i].toString());
		}
		int numSectors=numZsectors*numXsectors;
		for(int i=0;i<numSectors;i++){
			new Sector(din);	
		}
		int numPortals= (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numPortals:"+numPortals);
		portals=new Portal[numPortals];
		for(int i=0;i<numPortals;i++){
			portals[i]=new Portal(din,room_info,level);
		}
		din.readShort();

		staticMeshes=new RoomStaticMesh[numStaticMeshes];
		System.out.println("numStaticMeshes:"+numStaticMeshes);

		for(int i=0;i<numStaticMeshes;i++){
			staticMeshes[i]=new RoomStaticMesh(din);	
			System.out.println("StaticMesh "+i+": "+staticMeshes[i].toString());
		}
		System.out.println("numLayers "+numLayers);
		for(int i=0;i<numLayers;i++){
			new TR5RoomLayer(din);
		}
		//uint8_t Faces[(NumRoomRectangles * sizeof(tr_face4) + NumRoomTriangles * sizeof(tr_face3)];
		byte[] faces=new byte[numRoomRectangles*12+numRoomTriangles*8]; 
		din.readFully(faces);

		System.out.println("numVertices "+numVertices);
		for(int i=0;i<numVertices;i++){
			new TR5RoomVertex(din);	
		}
	}
	public RoomVertex[] roomVertices;
	public Face4[] rects;
	public Face3[] triangles;
	public RoomSprite[] roomSprites;

	public boolean isFilledWithWater(){
		return (flags & 1 << 0) != 0;
	}

	private void loadMesh(EndianDataInputStream din)throws IOException{
		int numVertices = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("NUMVERTICES: "+numVertices);
		roomVertices=new RoomVertex[numVertices];
		for(int i=0;i<numVertices;i++){
			roomVertices[i]=new RoomVertex(din);
		}

		int numRectangles = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("NUMRECTANGLES: "+numRectangles);
		rects=new Face4[numRectangles];
		for(int i=0;i<numRectangles;i++){
			rects[i]=new Face4(din);
			if(roomVertices[rects[i].v0]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[rects[i].v1]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[rects[i].v2]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[rects[i].v3]==null)
				throw new RuntimeException("vertex not found");
		}

		int numTriangles = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("NUMTRIANGLES: "+numTriangles);
		triangles=new Face3[numTriangles];
		for(int i=0;i<numTriangles;i++){
			triangles[i]=new Face3(din);
			if(roomVertices[triangles[i].v0]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[triangles[i].v1]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[triangles[i].v2]==null)
				throw new RuntimeException("vertex not found");
		}

		int numSprites = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("NUMSprites: "+numSprites);
		roomSprites=new RoomSprite[numSprites];
		for(int i=0;i<numSprites;i++){
			roomSprites[i]=new RoomSprite(din);
		}
	}
}
/*
virtual struct tr5_room // (variable length)
{
    char XELA[4];           // So-called "XELA landmark"
 
    uint32_t RoomDataSize;
 
    uint32_t Separator;     // 0xCDCDCDCD (4 bytes)
 
    uint32_t EndSDOffset;
    uint32_t StartSDOffset;
 
    uint32_t Separator;     // Either 0 or 0xCDCDCDCD
 
    uint32_t EndPortalOffset;
 
    tr5_room_info info;
 
    uint16_t NumZSectors;
    uint16_t NumXSectors;
 
    uint32_t RoomColour;   // In ARGB format!
 
    uint16_t NumLights;
    uint16_t NumStaticMeshes;
 
    uint8_t  ReverbInfo;
    uint8_t  AlternateGroup;
    uint16_t WaterScheme;
 
    uint32_t Filler[2];    // Both always 0x00007FFF
    uint32_t Separator[2]; // Both always 0xCDCDCDCD
    uint32_t Filler;       // Always 0xFFFFFFFF
 
    uint16_t AlternateRoom;
    uint16_t Flags;
 
    uint32_t Unknown1;
    uint32_t Unknown2;     // Always 0
    uint32_t Unknown3;     // Always 0
 
    uint32_t Separator;    // 0xCDCDCDCD
 
    uint16_t Unknown4;
    uint16_t Unknown5;
 
    float RoomX;
    float RoomY;
    float RoomZ;
 
    uint32_t Separator[4]; // Always 0xCDCDCDCD
    uint32_t Separator;    // 0 for normal rooms and 0xCDCDCDCD for null rooms
    uint32_t Separator;    // Always 0xCDCDCDCD
 
    uint32_t NumRoomTriangles;
    uint32_t NumRoomRectangles;
 
    uint32_t Separator;     // Always 0
 
    uint32_t LightDataSize;
    uint32_t NumLights2;    // Always same as NumLights
 
    uint32_t Unknown6;
 
    int32_t RoomYTop;
    int32_t RoomYBottom;
 
    uint32_t NumLayers;
 
    uint32_t LayerOffset;
    uint32_t VerticesOffset;
    uint32_t PolyOffset;
    uint32_t PolyOffset2;   // Same as PolyOffset
 
    uint32_t NumVertices;
 
    uint32_t Separator[4];  // Always 0xCDCDCDCD
 
    tr5_room_light Lights[NumLights];    // Data for the lights (88 bytes * NumRoomLights)
    tr_room_sector SectorList[NumXSectors * NumZSectors]; // List of sectors in this room
 
    uint16_t NumPortals;                 // Number of visibility portals to other rooms
    tr_room_portal Portals[NumPortals];  // List of visibility portals
 
    uint16_t Separator;  // Always 0xCDCD
 
    tr3_room_staticmesh StaticMeshes[NumStaticMeshes];   // List of static meshes
 
    tr5_room_layer Layers[NumLayers]; // Data for the room layers (volumes) (56 bytes * NumLayers)
 
    uint8_t Faces[(NumRoomRectangles * sizeof(tr_face4) + NumRoomTriangles * sizeof(tr_face3)];
 
    tr5_room_vertex Vertices[NumVertices];



*/

