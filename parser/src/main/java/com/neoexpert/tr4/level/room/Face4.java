package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class Face4{
	public int v0,v1,v2,v3;
	public int t;
	public Face4(EndianDataInputStream din)throws IOException{
		v0 = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		v1 = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		v2 = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		v3 = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		t = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
	}

	@Override
	public String toString(){
		return "v0: " + v0 + " - v1: " + v1 + " - v2: " + v2 + " - v3: "+v3+" - t: "+t;
	}

	public int getTexture(){
		return t  & ~(1 << 15);
	}
	public boolean isDoubleSided(){
		return (t & 1 << 15) != 0;
	}
}
/*
struct tr_face4    // 12 bytes
{
    uint16_t Vertices[4];
    uint16_t TRTexture;
};
 * */
