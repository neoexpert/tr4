package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import java.io.*;
import java.nio.*;
import com.neoexpert.*;



/**
 * Hello world!
 *
 */
public class Room
{
	public RoomInfo room_info;
	public Sector[] sectors;
	byte[] datawords;
	public int numZsectors;
	public int numXsectors;
	int numLights;
	public TRLight[] lights;
	int numStaticMeshes;
	public RoomStaticMesh[] staticMeshes;
	public Portal[] portals;
	public int alternate_room;
	int flags;

	int waterScheme;
	int reverbInfo;
	int alternate_group;

	public Room(EndianDataInputStream din,Level level)throws IOException
	{
		
		System.out.println("loading ROOM...");
		room_info=new RoomInfo(din,level.version);
		System.out.println("room_info:");
		System.out.println(room_info);

		int numDataWords=din.readInt();	
		System.out.println("numDataWords:"+numDataWords);
		datawords=new byte[numDataWords*2];
		for(int i=0;i<datawords.length;i++){
			datawords[i]=(byte)din.read();
		}
		System.out.println("datawords length:"+datawords.length);
		loadMesh(new EndianDataInputStream(new DataInputStream(new ByteArrayInputStream(datawords))));

		int numPortals= (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numPortals:"+numPortals);
		portals=new Portal[numPortals];
		for(int i=0;i<numPortals;i++){
			portals[i]=new Portal(din,room_info,level);
		}

		numZsectors = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numZsectors:"+numZsectors);
		numXsectors = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numXsectors:"+numXsectors);

		int numSectors=numZsectors*numXsectors;
		sectors=new Sector[numSectors];
		for(int i=0;i<numSectors;i++){
			sectors[i]=new Sector(din);	
		}
		//RoomColor:
		din.readInt();
		if(level.version==Level.TR2){
			din.readShort();
		}

		numLights = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		lights=new TRLight[numLights];
		System.out.println("numLights:"+numLights);
		for(int i=0;i<numLights;i++){
			TRLight light=level.createLight();
			light.read(din);
			lights[i]=light;
			System.out.println("Light "+i+lights[i].toString());
		}

		numStaticMeshes = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		staticMeshes=new RoomStaticMesh[numStaticMeshes];
		System.out.println("numStaticMeshes:"+numStaticMeshes);

		for(int i=0;i<numStaticMeshes;i++){
			staticMeshes[i]=new RoomStaticMesh(din);	
			System.out.println("StaticMesh "+i+": "+staticMeshes[i].toString());
		}

		
		alternate_room = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		flags = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		if(level.version==Level.TR3||level.version==Level.TR4){

			waterScheme=din.read();
			reverbInfo=din.read();
			alternate_group=din.read();
		}
	}
	public RoomVertex[] roomVertices;
	public Face4[] rects;
	public Face3[] triangles;
	public RoomSprite[] roomSprites;

	public boolean isFilledWithWater(){
		return (flags & 1 << 0) != 0;
	}

	private void loadMesh(EndianDataInputStream din)throws IOException{
		int numVertices = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("NUMVERTICES: "+numVertices);
		roomVertices=new RoomVertex[numVertices];
		for(int i=0;i<numVertices;i++){
			roomVertices[i]=new RoomVertex(din);
		}

		int numRectangles = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("NUMRECTANGLES: "+numRectangles);
		rects=new Face4[numRectangles];
		for(int i=0;i<numRectangles;i++){
			rects[i]=new Face4(din);
			if(roomVertices[rects[i].v0]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[rects[i].v1]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[rects[i].v2]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[rects[i].v3]==null)
				throw new RuntimeException("vertex not found");
		}

		int numTriangles = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("NUMTRIANGLES: "+numTriangles);
		triangles=new Face3[numTriangles];
		for(int i=0;i<numTriangles;i++){
			triangles[i]=new Face3(din);
			if(roomVertices[triangles[i].v0]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[triangles[i].v1]==null)
				throw new RuntimeException("vertex not found");
			if(roomVertices[triangles[i].v2]==null)
				throw new RuntimeException("vertex not found");
		}

		int numSprites = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("NUMSprites: "+numSprites);
		roomSprites=new RoomSprite[numSprites];
		for(int i=0;i<numSprites;i++){
			roomSprites[i]=new RoomSprite(din);
		}
	}
}
/*
virtual struct tr4_room  // (variable length)
{
    tr_room_info info;           // Where the room exists, in world coordinates

    uint32_t NumDataWords;       // Number of data words (uint16_t's)

    //Those data words must be parsed in order to interpret and construct the variable-length arrays of vertices, meshes, doors, and sectors.
    uint16_t Data[NumDataWords]; // The raw data from which the rest of this is derived

    tr_room_data RoomData;       // The room mesh

    uint16_t NumPortals;                 // Number of visibility portals to other rooms
    tr_room_portal Portals[NumPortals];  // List of visibility portals

    uint16_t NumZsectors;                                  // ``Width'' of sector list
    uint16_t NumXsectors;                                  // ``Height'' of sector list
    tr_room_sector SectorList[NumXsectors * NumZsectors]; // List of sectors in this room

    uint32_t RoomColour;        // In ARGB format!

    uint16_t NumLights;                 // Number of point lights in this room
    tr4_room_light Lights[NumLights];   // List of point lights

    uint16_t NumStaticMeshes;                           // Number of static meshes
    tr_room_staticmesh StaticMeshes[NumStaticMeshes];   // List of static meshes

    int16_t AlternateRoom;
    int16_t Flags;

    uint8_t WaterScheme;
    uint8_t ReverbInfo;

    uint8_t AlternateGroup;  // Replaces Filler from TR3
};
*/

