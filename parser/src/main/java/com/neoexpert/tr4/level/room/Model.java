package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class Model{
	public int id;
	public int numMeshes;
	public int startingMesh;
	public int meshTree;
	public int frameOffset;
	public int animation;
	public Model(EndianDataInputStream din)throws IOException{
		id=din.readInt();
		numMeshes= (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		startingMesh= (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		meshTree=din.readInt();
		frameOffset=din.readInt();
		animation = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("typeID: ");
		sb.append(id);
		sb.append(", frameOffset: ");
		sb.append(frameOffset);
		sb.append(", animation: ");
		sb.append(animation);
		return sb.toString();
	}
}
/*
struct tr_model  // 18 bytes
{
    uint32_t ID;           // Type Identifier (matched in Entities[])
    uint16_t NumMeshes;    // Number of meshes in this object
    uint16_t StartingMesh; // Stating mesh (offset into MeshPointers[])
    uint32_t MeshTree;     // Offset into MeshTree[]
    uint32_t FrameOffset;  // Byte offset into Frames[] (divide by 2 for Frames[i])
    uint16_t Animation;    // Offset into Animations[]
};
 * */
