package com.neoexpert.tr4.level;
import com.neoexpert.tr4.level.room.*;

import java.io.*;
import java.nio.*;

import java.util.zip.InflaterInputStream;
import java.util.zip.*;;


/**
 * Hello world!
 *
 */
public class TR4Level extends Level{
	protected TR4Level(EndianDataInputStream din, int version)throws IOException
	{
		super(version);

		int numRoomTextiles = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numRoomTextiles: "+ (int)numRoomTextiles);
		
		int numObjTextiles = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numObjTextiles: "+ (int)numObjTextiles);
		
		int numBumpTextiles = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numBumpTextiles: "+ (int)numBumpTextiles);
		
		int textile32_UncompSize=din.readInt();
		System.out.println("textile32_UncompSize: "+ (int)textile32_UncompSize);
		
		int textile32_CompSize=din.readInt();
		System.out.println("textile32_CompSize: "+ (int)textile32_CompSize);

		byte[] textile32_Compressed=new byte[textile32_CompSize];
		din.read(textile32_Compressed);

		ByteArrayInputStream bais = new ByteArrayInputStream(textile32_Compressed);
		InflaterInputStream	iis = new InflaterInputStream(bais);

		EndianDataInputStream dis = new EndianDataInputStream(iis);
		dis.order(ByteOrder.LITTLE_ENDIAN);
		int num_textile32=numRoomTextiles + numObjTextiles + numBumpTextiles;
		System.out.println("num_textile32: "+num_textile32);

		textures32=new Texture[num_textile32];;
		for(int i=0;i<num_textile32;i++){
			Texture tex=new Texture(256,256);
			textures32[i]=tex;
			int j=0;
			for(int x=0;x<256;x++){
				for(int y=0;y<256;y++){
					//textiles32[i][j]=dis.readInt();
					tex.pixels[x][y]=dis.readInt();
					j++;
				}
			}
		}

		int textile16_UncompSize=din.readInt();
		System.out.println("textile16_UncompSize: "+ (int)textile16_UncompSize);
		
		int textile16_CompSize=din.readInt();
		System.out.println("textile16_CompSize: "+ (int)textile16_CompSize);

		byte[] textile16_Compressed=new byte[textile16_CompSize];
		din.read(textile16_Compressed);

		int textile32Misc_UncompSize=din.readInt();
		System.out.println("textile32Misc_UncompSize: "+ (int)textile32Misc_UncompSize);
		
		int textile32Misc_CompSize=din.readInt();
		System.out.println("textile32Misc_CompSize: "+ (int)textile32Misc_CompSize);

		byte[] textile32Misc_Compressed=new byte[textile32Misc_CompSize];
		din.read(textile32Misc_Compressed);
		if(version==Level.TR5){
			int laraType=din.readShort();
			int weatherType=din.readShort();
			din.skipBytes(28);
		}

		int levelData_UncompSize=din.readInt();
		System.out.println("levelData_UncompSize: "+ (int)levelData_UncompSize);
		
		int levelData_CompSize=din.readInt();
		System.out.println("levelData_CompSize: "+ (int)levelData_CompSize);


		if(version==Level.TR5)
			levelData=new LevelData(din,this);
		else{
			byte[] levelData_Compressed=new byte[levelData_CompSize];
			din.read(levelData_Compressed);

			bais = new ByteArrayInputStream(levelData_Compressed);
			iis = new InflaterInputStream(bais);
			levelData=new LevelData(new EndianDataInputStream(iis),this);
		}


		if(version==Level.TR5)
			din.skipBytes(6);
		int numSamples=din.readInt();
		System.out.println("numSamples: "+ (int)numSamples);

		samples=new Sample[numSamples];
		for(int i=0;i<numSamples;i++){
			//System.out.println(i+":");
			samples[i]=new Sample(din);
		}
	}
	static int little2big(int i) {
		return (i&0xff)<<24 | (i&0xff00)<<8 | (i&0xff0000)>>8 | (i>>24)&0xff;
	}	
	public static void writeToFile(String name,byte[] bytes)throws IOException{
		FileOutputStream stream = new FileOutputStream(name);
		try {
			stream.write(bytes);
		} finally {
			stream.close();
		}	
	}
	public TRLight createLight(){
		if(version==TR4)
			return new TR4Light();
		else
			return new TR5Light();
	}
}
/*
uint8_t Textile32_Compressed[Textile32_CompSize]; // zlib-compressed 32-bit textures chunk (Textile32_CompSize bytes)
{
    tr4_textile32 Textile32[NumRoomTextiles + NumObjTextiles + NumBumpTextiles];
}
uint32_t Textile16_UncompSize; // uncompressed size (in bytes) of the 16-bit textures chunk (4 bytes)
uint32_t Textile16_CompSize; // compressed size (in bytes) of the 16-bit textures chunk (4 bytes)
uint8_t Textile16_Compressed[Textile32_CompSize]; // zlib-compressed 16-bit textures chunk (Textile16_CompSize bytes)
{
    tr_textile16 Textile16[NumRoomTextiles + NumObjTextiles + NumBumpTextiles];
}
uint32_t Textile32Misc_UncompSize; // uncompressed size (in bytes) of the 32-bit misc textures chunk (4 bytes), should always be 524288
uint32_t Textile32Misc_CompSize; // compressed size (in bytes) of the 32-bit misc textures chunk (4 bytes)
uint8_t Textile32Misc_Compressed[Textile32Misc_CompSize]; // zlib-compressed 32-bit misc textures chunk (Textile32Misc_CompSize bytes)
{
    tr4_textile32 Textile32Misc[2];
}
uint32_t LevelData_UncompSize; // uncompressed size (in bytes) of the level data chunk (4 bytes)
uint32_t LevelData_CompSize; // compressed size (in bytes) of the level data chunk (4 bytes)
uint8_t LevelData_Compressed[LevelData_CompSize]; // zlib-compressed level data chunk (LevelData_CompSize bytes)
{
    uint32_t Unused; // 32-bit unused value, always 0 (4 bytes)
    uint16_t NumRooms; // number of rooms (2 bytes)
    tr4_room Rooms[NumRooms]; // room list (variable length)
    uint32_t NumFloorData; // number of floor data uint16_t's to follow (4 bytes)
    uint16_t FloorData[NumFloorData]; // floor data (NumFloorData * 2 bytes)
    uint32_t NumMeshData; // number of uint16_t's of mesh data to follow (=Meshes[]) (4 bytes)
    tr4_mesh Meshes[NumMeshPointers]; // note that NumMeshPointers comes AFTER Meshes[]
    uint32_t NumMeshPointers; // number of mesh pointers to follow (4 bytes)
    uint32_t MeshPointers[NumMeshPointers]; // mesh pointer list (NumMeshPointers * 4 bytes)
    uint32_t NumAnimations; // number of animations to follow (4 bytes)
    tr4_animation Animations[NumAnimations]; // animation list (NumAnimations * 40 bytes)
    uint32_t NumStateChanges; // number of state changes to follow (4 bytes)
    tr_state_change StateChanges[NumStateChanges]; // state-change list (NumStructures * 6 bytes)
    uint32_t NumAnimDispatches; // number of animation dispatches to follow (4 bytes)
    tr_anim_dispatch AnimDispatches[NumAnimDispatches]; // animation-dispatch list list (NumAnimDispatches * 8 bytes)
    uint32_t NumAnimCommands; // number of animation commands to follow (4 bytes)
    tr_anim_command AnimCommands[NumAnimCommands]; // animation-command list (NumAnimCommands * 2 bytes)
    uint32_t NumMeshTrees; // number of MeshTrees to follow (4 bytes)
    tr_meshtree_node MeshTrees[NumMeshTrees]; // MeshTree list (NumMeshTrees * 4 bytes)
    uint32_t NumFrames; // number of words of frame data to follow (4 bytes)
    uint16_t Frames[NumFrames]; // frame data (NumFrames * 2 bytes)
    uint32_t NumMoveables; // number of moveables to follow (4 bytes)
    tr_model Moveables[NumMoveables]; // moveable list (NumMoveables * 18 bytes)
    uint32_t NumStaticMeshes; // number of StaticMesh data records to follow (4 bytes)
    tr_staticmesh StaticMeshes[NumStaticMeshes]; // StaticMesh data (NumStaticMesh * 32 bytes)
    uint8_t SPR[3]; // S P R (0x53, 0x50, 0x52)
    uint32_t NumSpriteTextures; // number of sprite textures to follow (4 bytes)
    tr_sprite_texture SpriteTextures[NumSpriteTextures]; // sprite texture list (NumSpriteTextures * 16 bytes)
    uint32_t NumSpriteSequences; // number of sprite sequences records to follow (4 bytes)
    tr_sprite_sequence SpriteSequences[NumSpriteSequences]; // sprite sequence data (NumSpriteSequences * 8 bytes)
    uint32_t NumCameras; // number of camera data records to follow (4 bytes)
    tr_camera Cameras[NumCameras]; // camera data (NumCameras * 16 bytes)
    uint32_t NumFlybyCameras; // number of flyby camera data records to follow (4 bytes)
    tr4_flyby_camera FlybyCameras[NumFlybyCameras]; // flyby camera data (NumFlybyCameras * 40 bytes)
    uint32_t NumSoundSources; // number of sound source data records to follow (4 bytes)
    tr_sound_source SoundSources[NumSoundSources]; // sound source data (NumSoundSources * 16 bytes)
    uint32_t NumBoxes; // number of box data records to follow (4 bytes)
    tr2_box Boxes[NumBoxes]; // box data (NumBoxes * 8 bytes)
    uint32_t NumOverlaps; // number of overlap records to follow (4 bytes)
    uint16_t Overlaps[NumOverlaps]; // overlap data (NumOverlaps * 2 bytes)
    int16_t Zones[10*NumBoxes]; // zone data (NumBoxes * 20 bytes)
    uint32_t NumAnimatedTextures; // number of animated texture records to follow (4 bytes)
    uint16_t AnimatedTextures[NumAnimatedTextures]; // animated texture data (NumAnimatedTextures * 2 bytes)
    uint8_t AnimatedTexturesUVCount;
    uint32_t NumObjectTextures; // number of object textures to follow (4 bytes) (after AnimatedTextures in TR3)
    tr4_object_texture ObjectTextures[NumObjectTextures]; // object texture list (NumObjectTextures * 38 bytes)
    uint32_t NumItems; // number of items to follow (4 bytes)
    tr4_entity Items[NumItems]; // item list (NumItems * 24 bytes)
    uint32_t NumAIObjects; // number of AI objects to follow (4 bytes)
    tr4_ai_object AIObjects[NumAIObjects]; // AI objects list (NumAIObjects * 24 bytes)
    uint16_t NumDemoData; // number of demo data records to follow (2 bytes)
    uint8_t DemoData[NumDemoData]; // demo data (NumDemoData bytes)
    int16_t SoundMap[370]; // sound map (740 bytes)
    uint32_t NumSoundDetails; // number of sound-detail records to follow (4 bytes)
    tr3_sound_details SoundDetails[NumSoundDetails]; // sound-detail list (NumSoundDetails * 8 bytes)
    uint32_t NumSampleIndices; // number of sample indices to follow (4 bytes)  +
    uint32_t SampleIndices[NumSampleIndices]; // sample indices (NumSampleIndices * 4 bytes)
}
uint32_t NumSamples; // number of sound samples (4 bytes)
tr4_sample Samples[NumSamples]; // sound samples (this is the last part, so you can simply read until EOF)
*/

