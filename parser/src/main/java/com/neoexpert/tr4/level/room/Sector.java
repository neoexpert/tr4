package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.tr4.level.*;
import java.io.*;

public class Sector{
	public int fdIndex;
	public int boxIndex;
	public int roomBelow;
	public int floor;
	public int roomAbove;
	public int ceiling;
	public Sector(EndianDataInputStream din)throws IOException{
		fdIndex=din.readShort();
		boxIndex=din.readShort();
		roomBelow=din.readByte()&0xFF;
		floor=din.readByte();
		roomAbove=din.readByte()&0xFF;
		ceiling=din.readByte();
	}
}
/*
struct tr_room_sector // 8 bytes
{
    uint16_t FDindex;    // Index into FloorData[]
    uint16_t BoxIndex;   // Index into Boxes[] (-1 if none)
    uint8_t  RoomBelow;  // 255 is none
    int8_t   Floor;      // Absolute height of floor
    uint8_t  RoomAbove;  // 255 if none
    int8_t   Ceiling;    // Absolute height of ceiling
};
 * */
