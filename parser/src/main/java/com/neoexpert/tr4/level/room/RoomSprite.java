package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class RoomSprite{
	int v;
	int t;
	public RoomSprite(EndianDataInputStream din)throws IOException{
		v=din.readChar();
		t=din.readChar();
	}

	@Override
	public String toString(){
		return "v: " + v + " - t: "+t;
	}
}
/*
struct tr_room_sprite  // 4 bytes
{
    int16_t Vertex;       // Offset into vertex list
    int16_t TRTexture;      // Offset into sprite texture list
};
 * */
