package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class Entity{
	public int typeID;
	public int room;
	public int x,y,z;
	public char angle;
	public char intensity;
	public char ocb;
	public char flags;
	public Entity(EndianDataInputStream din)throws IOException{
		typeID = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		room = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		x=din.readInt();
		y=din.readInt();
		z=din.readInt();
		angle = (char)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		intensity = (char)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		ocb = (char)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		flags = (char)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("typeID: ");
		sb.append(typeID);
		sb.append(", x: ");
		sb.append(x);
		sb.append(", y: ");
		sb.append(y);
		sb.append(", z: ");
		sb.append(z);
		return sb.toString();
	}
}
/*
struct tr4_entity // 24 bytes
{
    int16_t TypeID;
    int16_t Room;
    int32_t x;
    int32_t y;
    int32_t z;
    int16_t Angle;
    int16_t Intensity1;
    int16_t OCB;        // Replaces Intensity2, see further for explanations.
    uint16_t Flags;
};
 * */
