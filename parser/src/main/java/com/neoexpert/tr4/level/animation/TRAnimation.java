package com.neoexpert.tr4.level.animation;
import com.neoexpert.tr4.level.*;
import java.io.*;

public class TRAnimation {
	public int frameOffset;
	public byte frameRate;
	public byte frameSize;
	public int state_ID;
	public int speed;
	public int accel;
	public int speedLateral;
	public int accelLateral;
	public int frameStart;
	public int frameEnd;
	public int nextAnimation;
	public int nextFrame;
	public int numStateChanges;
	public int stateChangeOffset;
	public int numAnimCommands;
	public int animCommand;
	public TRAnimation(EndianDataInputStream din, int version)throws IOException{
		frameOffset=din.readInt();
		frameRate=(byte)din.read();
		frameSize=(byte)din.read();
		state_ID= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		speed=din.readInt();
		accel=din.readInt();
		if(version==Level.TR4||version==Level.TR5){
			speedLateral=din.readInt();
			accelLateral=din.readInt();
		}
		frameStart= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		frameEnd= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		nextAnimation= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		nextFrame= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		numStateChanges= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		stateChangeOffset= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		numAnimCommands= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		animCommand = ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("animCommand:"+animCommand);
		sb.append(",");
		sb.append("nextAnim:"+nextAnimation);
		return sb.toString();
	}
}
/*
struct tr4_animation // 40 bytes
{
    uint32_t  FrameOffset;
     uint8_t  FrameRate;
     uint8_t  FrameSize;

    uint16_t  State_ID;

       fixed  Speed;
       fixed  Accel;
       fixed  SpeedLateral; // New field
       fixed  AccelLateral; // New field

    uint16_t  FrameStart;
    uint16_t  FrameEnd;
    uint16_t  NextAnimation;
    uint16_t  NextFrame;

    uint16_t  NumStateChanges;
    uint16_t  StateChangeOffset;

    uint16_t  NumAnimCommands;
    uint16_t  AnimCommand;
};
 * */
