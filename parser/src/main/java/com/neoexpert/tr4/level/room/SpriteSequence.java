package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class SpriteSequence{
	public int spriteID;
	public int negLength;
	public int offset;
	public SpriteSequence(EndianDataInputStream din)throws IOException{
		spriteID=din.readInt();
		negLength= (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		offset = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
	}
}
/*
struct tr_sprite_sequence  // 8 bytes
{
    int32_t SpriteID;       // Sprite identifier
    int16_t NegativeLength; // Negative of ``how many sprites are in this sequence''
    int16_t Offset;         // Where (in sprite texture list) this sequence starts
};
 * */
