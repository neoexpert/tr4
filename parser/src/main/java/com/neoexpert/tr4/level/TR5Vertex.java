package com.neoexpert.tr4.level;

import com.neoexpert.*;
import com.neoexpert.tr4.level.*;
import java.io.*;

public class TR5Vertex{
	public float x,y,z;
	public TR5Vertex(EndianDataInputStream din)throws IOException{
		x = din.readFloat();
		y = din.readFloat();
		z = din.readFloat();
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(");
		sb.append(x);
		sb.append(",");
		sb.append(y);
		sb.append(",");
		sb.append(z);
		sb.append(")");
		return sb.toString();
	}
}
/*
struct tr_vertex   // 6 bytes
{
    int16_t x;
    int16_t y;
    int16_t z;
};
 * */
