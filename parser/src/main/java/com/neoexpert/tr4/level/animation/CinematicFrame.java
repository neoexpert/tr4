package com.neoexpert.tr4.level.animation;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class CinematicFrame{
	public CinematicFrame(EndianDataInputStream din)throws IOException{
    		//int16_t targetX; // Camera look at position about X axis,
		din.readShort();
    		//int16_t targetY; // Camera look at position about Y axis
		din.readShort();
    		//int16_t target2; // Camera look at position about Z axis
		din.readShort();
    		//int16_t posZ;    // Camera position about Z axis
		din.readShort();
    		//int16_t posY;    // Camera position relative to something (see posZ)
		din.readShort();
    		//int16_t posX;    // Camera position relative to something (see posZ)
		din.readShort();
    		//int16_t fov;
		din.readShort();
    		//int16_t roll;    // Rotation about X axis
		din.readShort();
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		return sb.toString();
	}
}
/*
struct tr_cinematic_frame // 16 bytes
{
    int16_t targetX; // Camera look at position about X axis,
    int16_t targetY; // Camera look at position about Y axis
    int16_t target2; // Camera look at position about Z axis
    int16_t posZ;    // Camera position about Z axis
    int16_t posY;    // Camera position relative to something (see posZ)
    int16_t posX;    // Camera position relative to something (see posZ)
    int16_t fov;
    int16_t roll;    // Rotation about X axis
};
 * */
