package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class Color{
	public Color(int r, int g, int b){
		this.r=r;
		this.g=g;
		this.b=b;
	}
	public final int r,g,b;
	public Color(EndianDataInputStream din)throws IOException{
		r=(byte)din.readByte()&0xff;
		g=(byte)din.readByte()&0xff;
		b=(byte)din.readByte()&0xff;
	}
}
