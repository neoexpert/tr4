package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class RoomStaticMesh{
	public int x,y,z;
	public int rotation;
	public int intensity;
	public int meshID;

	public RoomStaticMesh(EndianDataInputStream din)throws IOException{
		x=din.readInt();
		y=din.readInt();
		z=din.readInt();

		rotation = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		intensity = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		din.readChar();
		meshID = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(");
		sb.append(x);
		sb.append(",");
		sb.append(y);
		sb.append(",");
		sb.append(z);
		sb.append(") ");
		return sb.toString();
	}
	
}
/*
tr3_room_staticmesh  // 20 bytes
{
    uint32_t x, y, z;    // Absolute position in world coordinates
    uint16_t Rotation;
    uint16_t Colour;     // 15-bit colour
    uint16_t Unused;     // Not used!
    uint16_t MeshID;     // Which StaticMesh item to draw
};
 * */
