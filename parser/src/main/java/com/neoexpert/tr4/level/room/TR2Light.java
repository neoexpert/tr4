package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.palette.*;

import com.neoexpert.*;
import java.io.*;

public class TR2Light extends TRLight{
	int intensity;
	int intensity2;
	int fade;
	int fade2;
	public void read(EndianDataInputStream din)throws IOException{
		x=din.readInt();
		y=din.readInt();
		z=din.readInt();
		intensity=din.readShort();
		intensity2=din.readShort();
		fade=din.readInt();
		fade2=din.readInt();
		color=new Color(255,255,255);
		type=1;
	}
}
