package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class TR5RoomVertex{
	public final TR5Vertex vertex;
	public final TR5Vertex normal;
	public final int color;
	public TR5RoomVertex(EndianDataInputStream din)throws IOException{
		vertex=new TR5Vertex(din);
		normal=new TR5Vertex(din);
		color=din.readInt();
	}

}
