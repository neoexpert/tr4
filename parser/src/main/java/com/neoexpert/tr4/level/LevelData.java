package com.neoexpert.tr4.level;

import java.io.*;
import java.util.*;
import java.nio.*;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.level.animation.*;
import com.neoexpert.tr4.level.mesh.*;



/**
 * Hello world!
 *
 */
public class LevelData
{
	int numRooms;
	public Room[] rooms;
	public TR5Room[] tr5rooms;
	public short[] floorData;
	int numFloorData;
	int numMeshPointers;
	public int[] meshPointers;
	public Map<Integer,Mesh> meshes=new HashMap<Integer,Mesh>();
	int numAnimations;
	public TRAnimation[] animations;
	int numStateChanges;
	public StateChange[] stateChanges;
	public int numAnimDispatches;
	public AnimDispatch[] animDispatches;
	int numAnimCommands;
	public char[] animCommands;
	int numMeshTrees;
	public int[] meshTrees;
	int numFrames;
	public byte[] frames;
	int numMoveables;
	public Model[] moveables;
	int numStaticMeshes;
	public StaticMesh[] staticMeshes;
	int numSpriteTextures;
	public SpriteTexture[] spriteTextures;
	int numSpriteSequences;
	public SpriteSequence[] spriteSequences;
	int numCameras;
	Camera[] cameras;
	int numFlybyCameras;
	public FlybyCamera[] flybyCameras;
	int numSoundSources;
	SoundSource[] soundSources;
	int numBoxes;
	Box[] boxes;
	int numOverlaps;
	char[] overlaps;
	char[] zones;
	int numAnimatedTextures;
	public int[] animatedTextures;
	byte animatedTexturesUVCount;
	int numObjectTextures;
	public ObjectTexture[] objectTextures;
	int numItems;
	public Entity[] items;
	public Entity lara;
	int numAIObjects;
	AIObject[] aiObjects;
	int numDemoData;
	int[] demoData;
	char[] soundMap;
	int numSoundDetails;
	SoundDetail[] soundDetails;
	int numSampleIndices;
	int[] sampleIndices;

	public LevelData(Level level){
	}
	public LevelData(EndianDataInputStream din,Level level)throws IOException
	{
		din.order(ByteOrder.LITTLE_ENDIAN);
		int unused=din.readInt();
		if(unused!=0)throw new RuntimeException("LevelData corrupt");
		if(level.version==Level.TR5)
		{
			numRooms= din.readInt();
			System.out.println("numRooms: "+numRooms);
			tr5rooms=new TR5Room[numRooms];
			for(int i=0;i<numRooms;i++){
				tr5rooms[i]=new TR5Room(din,level);
			}
		}
		else{
			numRooms= (din.read() & 0xff) | ((din.read() & 0xff)<<8);
			System.out.println("numRooms: "+numRooms);
			rooms=new Room[numRooms];
			for(int i=0;i<numRooms;i++){
				rooms[i]=new Room(din,level);
			}
		}

		numFloorData= din.readInt();
		floorData=new short[numFloorData];
		System.out.println("numFloorData: "+numFloorData);
		for(int i=0;i<numFloorData;i++){
			floorData[i]=din.readShort();
		}


		int numMeshData= din.readInt();
		System.out.println("numMeshData: "+numMeshData);
		byte[] meshData=new byte[numMeshData*2];

		din.readFully(meshData);



		numMeshPointers= din.readInt();
		meshPointers=new int[numMeshPointers];
		//for(int i=0;i<numMeshPointers;i++)

		System.out.println("numMeshPointers: "+numMeshPointers);

		for(int i=0;i<numMeshPointers;i++){
			int pointer=din.readInt();
			meshPointers[i]=pointer;
			ByteArrayInputStream bais = new ByteArrayInputStream(meshData);

			DataInputStream dismeshes=new DataInputStream(bais);
			EndianDataInputStream dinm = new EndianDataInputStream(dismeshes);
			dinm.skipBytes(pointer);
			meshes.put(pointer,new Mesh(dinm,level.version));
		}

		numAnimations= din.readInt();
		System.out.println("numAnimations: "+numAnimations);
		animations=new TRAnimation[numAnimations];
		for(int i=0;i<numAnimations;i++){
			animations[i]=new TRAnimation(din,level.version);
		}
		numStateChanges= din.readInt();
		stateChanges=new StateChange[numStateChanges];
		System.out.println("numStateChanges: "+numStateChanges);
		for(int i=0;i<numStateChanges;i++){
			stateChanges[i]=new StateChange(din);	
		}

		numAnimDispatches= din.readInt();
		animDispatches=new AnimDispatch[numAnimDispatches];
		System.out.println("numAnimDispatches: "+numAnimDispatches);
		for(int i=0;i<numAnimDispatches;i++){
			animDispatches[i]=new AnimDispatch(din);	
		}


		numAnimCommands= din.readInt();
		animCommands=new char[numAnimCommands];
		System.out.println("numAnimCommands: "+numAnimCommands);
		for(int i=0;i<numAnimCommands;i++){
			animCommands[i]=din.readChar();	
		}

		numMeshTrees = din.readInt();
		meshTrees=new int[numMeshTrees];
		System.out.println("numMeshTrees: "+numMeshTrees);
		for(int i=0;i<numMeshTrees;i++){
			meshTrees[i]=din.readInt();
		}

		numFrames = din.readInt();
		frames=new byte[numFrames*2];
		System.out.println("numFrames: "+numFrames);
		din.readFully(frames);
		/*
		for(int i=0;i<numFrames*2;i++){

			frames[i]=(byte)din.read();
		}*/

		numMoveables = din.readInt();
		moveables=new Model[numMoveables];
		System.out.println("numMoveables: "+numMoveables);
		for(int i=0;i<numMoveables;i++){
			moveables[i]=new Model(din);
			if(level.version==Level.TR5)
				din.readShort();
		}

		numStaticMeshes = din.readInt();
		staticMeshes=new StaticMesh[numStaticMeshes];
		System.out.println("numStaticMeshes: "+numStaticMeshes);
		for(int i=0;i<numStaticMeshes;i++){
			staticMeshes[i]=new StaticMesh(din);
		}

		if(din.read()!='S') //S
			throw new RuntimeException("SPR not found");
		din.read(); //P
		din.read(); //R
		if(level.version==Level.TR5)
			if(din.read()!=0) //S
				throw new RuntimeException("SPR not found");

		numSpriteTextures = din.readInt();
		spriteTextures=new SpriteTexture[numSpriteTextures];
		System.out.println("numSpriteTextures: "+numSpriteTextures);
		for(int i=0;i<numSpriteTextures;i++){
			spriteTextures[i]=new SpriteTexture(din);
		}

		numSpriteSequences = din.readInt();
		spriteSequences=new SpriteSequence[numSpriteSequences];
		System.out.println("numSpriteSequences: "+numSpriteSequences);
		for(int i=0;i<numSpriteSequences;i++){
			spriteSequences[i]=new SpriteSequence(din);
		}

		numCameras = din.readInt();
		cameras=new Camera[numCameras];
		System.out.println("numCameras: "+numCameras);
		for(int i=0;i<numCameras;i++){
			cameras[i]=new Camera(din);
		}

		numFlybyCameras = din.readInt();
		flybyCameras=new FlybyCamera[numFlybyCameras];
		System.out.println("numFlybyCameras: "+numFlybyCameras);
		for(int i=0;i<numFlybyCameras;i++){
			flybyCameras[i]=new FlybyCamera(din);
		}

		numSoundSources	= din.readInt();
		soundSources=new SoundSource[numSoundSources];
		System.out.println("numSoundSources: "+numSoundSources);
		for(int i=0;i<numSoundSources;i++){
			soundSources[i]=new SoundSource(din);
		}

		numBoxes = din.readInt();
		boxes=new Box[numBoxes];
		System.out.println("numBoxes: "+numBoxes);
		for(int i=0;i<numBoxes;i++){
			boxes[i]=new Box(din);
		}

		numOverlaps = din.readInt();
		overlaps=new char[numOverlaps];
		System.out.println("numOverlaps: "+numOverlaps);
		for(int i=0;i<numOverlaps;i++){
			overlaps[i]=din.readChar();
		}

		int numZones=numBoxes*10;
		zones=new char[numZones];
		for(int i=0;i<numZones;i++){
			zones[i]=din.readChar();
		}

		numAnimatedTextures = din.readInt();
		animatedTextures=new int[numAnimatedTextures];
		System.out.println("numAnimatedTextures: "+numAnimatedTextures);
		for(int i=0;i<numAnimatedTextures;i++){
			animatedTextures[i]=din.readShort();
		}
		animatedTexturesUVCount=(byte)din.read();
		din.read();//T
		din.read();//E
		din.read();//X
		if(level.version==Level.TR5)
			if(din.read()!=0)
				throw new RuntimeException("SPR not found");

		numObjectTextures=din.readInt();
		System.out.println("numObjectTextures: "+numObjectTextures);
		objectTextures=new ObjectTexture[numObjectTextures];
		for(int i=0;i<numObjectTextures;i++){
			objectTextures[i]=new ObjectTexture(din,level.version);
			if(level.version==Level.TR5)
				din.readShort();
		}

		numItems=din.readInt();
		System.out.println("numItems: "+numItems);
		items=new Entity[numItems];
		for(int i=0;i<numItems;i++){
			items[i]=new Entity(din);
			if(items[i].typeID==0)
			{
				if(lara!=null)
					throw new RuntimeException("lara was already found");
				lara=items[i];
				System.out.println("lara found: "+lara);
			}
		}

		numAIObjects=din.readInt();
		System.out.println("numAIObjects: "+numAIObjects);
		aiObjects=new AIObject[numAIObjects];
		for(int i=0;i<numAIObjects;i++){
			aiObjects[i]=new AIObject(din);
		}

		numDemoData=din.readShort();
		System.out.println("numDemoData: "+numDemoData);
		demoData=new int[numDemoData];
		for(int i=0;i<numDemoData;i++){
			demoData[i]=din.readChar();
		}

		if(level.version==Level.TR5)
			soundMap=new char[450];
		else
			soundMap=new char[370];
		for(int i=0;i<soundMap.length;i++){
			soundMap[i]=din.readChar();
		}

		numSoundDetails=din.readInt();
		System.out.println("numSoundDetails: "+numSoundDetails);
		soundDetails=new SoundDetail[numSoundDetails];
		for(int i=0;i<numSoundDetails;i++){
			soundDetails[i]=new SoundDetail(din);
		}

		numSampleIndices=din.readInt();
		System.out.println("numSampleIndices: "+numSampleIndices);
		sampleIndices=new int[numSampleIndices];
		for(int i=0;i<numSampleIndices;i++){
			sampleIndices[i]=din.readInt();
		}
	}
	public Frame getFrame(int offset, float[][] rotations, int meshes_count){
		try{
			ByteArrayInputStream bais = new ByteArrayInputStream(frames);

			DataInputStream dismeshes=new DataInputStream(bais);
			EndianDataInputStream dinm = new EndianDataInputStream(dismeshes);
			dinm.skipBytes(offset);


			return new Frame(dinm,rotations, meshes_count);
		}
		catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	static int little2big(int i) {
		return (i&0xff)<<24 | (i&0xff00)<<8 | (i&0xff0000)>>8 | (i>>24)&0xff;
	}	
	public static String intToString(int n){
		return new String(ByteBuffer.allocate(4).putInt(n).array());
	}
	public static void writeToFile(String name,byte[] bytes)throws IOException{
		FileOutputStream stream = new FileOutputStream(name);
		try {
			stream.write(bytes);
		} finally {
			stream.close();
		}	
	}
}
/*
    uint32_t Unused; // 32-bit unused value, always 0 (4 bytes)
    uint16_t NumRooms; // number of rooms (2 bytes)
    tr4_room Rooms[NumRooms]; // room list (variable length)
    uint32_t NumFloorData; // number of floor data uint16_t's to follow (4 bytes)
    uint16_t FloorData[NumFloorData]; // floor data (NumFloorData * 2 bytes)
    uint32_t NumMeshData; // number of uint16_t's of mesh data to follow (=Meshes[]) (4 bytes)
    tr4_mesh Meshes[NumMeshPointers]; // note that NumMeshPointers comes AFTER Meshes[]
    uint32_t NumMeshPointers; // number of mesh pointers to follow (4 bytes)
    uint32_t MeshPointers[NumMeshPointers]; // mesh pointer list (NumMeshPointers * 4 bytes)
    uint32_t NumAnimations; // number of animations to follow (4 bytes)
    tr4_animation Animations[NumAnimations]; // animation list (NumAnimations * 40 bytes)
    uint32_t NumStateChanges; // number of state changes to follow (4 bytes)
    tr_state_change StateChanges[NumStateChanges]; // state-change list (NumStructures * 6 bytes)
    uint32_t NumAnimDispatches; // number of animation dispatches to follow (4 bytes)
    tr_anim_dispatch AnimDispatches[NumAnimDispatches]; // animation-dispatch list list (NumAnimDispatches * 8 bytes)
    uint32_t NumAnimCommands; // number of animation commands to follow (4 bytes)
    tr_anim_command AnimCommands[NumAnimCommands]; // animation-command list (NumAnimCommands * 2 bytes)
    uint32_t NumMeshTrees; // number of MeshTrees to follow (4 bytes)
    tr_meshtree_node MeshTrees[NumMeshTrees]; // MeshTree list (NumMeshTrees * 4 bytes)
    uint32_t NumFrames; // number of words of frame data to follow (4 bytes)
    uint16_t Frames[NumFrames]; // frame data (NumFrames * 2 bytes)
    uint32_t NumMoveables; // number of moveables to follow (4 bytes)
    tr_model Moveables[NumMoveables]; // moveable list (NumMoveables * 18 bytes)
    uint32_t NumStaticMeshes; // number of StaticMesh data records to follow (4 bytes)
    tr_staticmesh StaticMeshes[NumStaticMeshes]; // StaticMesh data (NumStaticMesh * 32 bytes)
    uint8_t SPR[3]; // S P R (0x53, 0x50, 0x52)
    uint32_t NumSpriteTextures; // number of sprite textures to follow (4 bytes)
    tr_sprite_texture SpriteTextures[NumSpriteTextures]; // sprite texture list (NumSpriteTextures * 16 bytes)
    uint32_t NumSpriteSequences; // number of sprite sequences records to follow (4 bytes)
    tr_sprite_sequence SpriteSequences[NumSpriteSequences]; // sprite sequence data (NumSpriteSequences * 8 bytes)
    uint32_t NumCameras; // number of camera data records to follow (4 bytes)
    tr_camera Cameras[NumCameras]; // camera data (NumCameras * 16 bytes)
    uint32_t NumFlybyCameras; // number of flyby camera data records to follow (4 bytes)
    tr4_flyby_camera FlybyCameras[NumFlybyCameras]; // flyby camera data (NumFlybyCameras * 40 bytes)
    uint32_t NumSoundSources; // number of sound source data records to follow (4 bytes)
    tr_sound_source SoundSources[NumSoundSources]; // sound source data (NumSoundSources * 16 bytes)
    uint32_t NumBoxes; // number of box data records to follow (4 bytes)
    tr2_box Boxes[NumBoxes]; // box data (NumBoxes * 8 bytes)
    uint32_t NumOverlaps; // number of overlap records to follow (4 bytes)
    uint16_t Overlaps[NumOverlaps]; // overlap data (NumOverlaps * 2 bytes)
    int16_t Zones[10*NumBoxes]; // zone data (NumBoxes * 20 bytes)
    uint32_t NumAnimatedTextures; // number of animated texture records to follow (4 bytes)
    uint16_t AnimatedTextures[NumAnimatedTextures]; // animated texture data (NumAnimatedTextures * 2 bytes)
    uint8_t AnimatedTexturesUVCount;
    uint32_t NumObjectTextures; // number of object textures to follow (4 bytes) (after AnimatedTextures in TR3)
    tr4_object_texture ObjectTextures[NumObjectTextures]; // object texture list (NumObjectTextures * 38 bytes)
    uint32_t NumItems; // number of items to follow (4 bytes)
    tr4_entity Items[NumItems]; // item list (NumItems * 24 bytes)
    uint32_t NumAIObjects; // number of AI objects to follow (4 bytes)
    tr4_ai_object AIObjects[NumAIObjects]; // AI objects list (NumAIObjects * 24 bytes)
    uint16_t NumDemoData; // number of demo data records to follow (2 bytes)
    uint8_t DemoData[NumDemoData]; // demo data (NumDemoData bytes)
    int16_t SoundMap[370]; // sound map (740 bytes)
    uint32_t NumSoundDetails; // number of sound-detail records to follow (4 bytes)
    tr3_sound_details SoundDetails[NumSoundDetails]; // sound-detail list (NumSoundDetails * 8 bytes)
    uint32_t NumSampleIndices; // number of sample indices to follow (4 bytes)  +
    uint32_t SampleIndices[NumSampleIndices]; // sample indices (NumSampleIndices * 4 bytes)
*/

