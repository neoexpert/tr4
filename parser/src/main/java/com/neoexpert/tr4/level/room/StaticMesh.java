package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class StaticMesh{
	public BoundingBox visibilityBox;
	public BoundingBox collisionsBox;
	public int id;
	public int mesh;
	public char flags;

	public StaticMesh(EndianDataInputStream din)throws IOException{
		id=din.readInt();
		mesh = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		visibilityBox=new BoundingBox(din);
		collisionsBox=new BoundingBox(din);
		flags=din.readChar();
	}

	
}
/*
struct tr_staticmesh   // 32 bytes
{
    uint32_t        ID;   // Static Mesh Identifier
    uint16_t        Mesh; // Mesh (offset into MeshPointers[])
    tr_bounding_box VisibilityBox;
    tr_bounding_box CollisionBox;
    uint16_t        Flags;
};
 * */
