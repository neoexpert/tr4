package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.palette.*;

import com.neoexpert.*;
import java.io.*;

public class TR3Light extends TRLight{
	TRColour4 tr3_color;
	int intensity;
	int fade;
	public void read(EndianDataInputStream din)throws IOException{
		x=din.readInt();
		y=din.readInt();
		z=din.readInt();
		tr3_color=new TRColour4(din);
		color=new Color(tr3_color.r,tr3_color.g,tr3_color.b);
		intensity=din.readInt();
		fade=din.readInt();
		type=1;
	}
}
