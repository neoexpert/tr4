package com.neoexpert.tr4.level;

import java.io.*;
import java.nio.*;

public class Sample{
	byte[] bytes;
	public Sample(EndianDataInputStream din)throws IOException{
		int uSize=din.readInt();
		//System.out.println("uSize: "+ (int)uSize);

		int cSize=din.readInt();
		//System.out.println("cSize: "+ (int)cSize);

		bytes=new byte[cSize];
		din.read(bytes);
	}
}
