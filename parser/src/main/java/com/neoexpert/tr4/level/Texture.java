package com.neoexpert.tr4.level;

import com.neoexpert.*;
import com.neoexpert.tr4.level.*;
import java.io.*;

public class Texture{
	public Texture(int w, int h){
		pixels=new int[w][h];
	}
	public int[][] pixels;
}
