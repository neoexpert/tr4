package com.neoexpert.tr4.level.mesh;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;
import java.nio.*;

public class Mesh{
	public Vertex centre;
	public int collRadius;
	public Vertex[] vertices;
	public Vertex[] normals;
	public char[] lights;
	public MeshFace4[] textRects;
	public MeshFace3[] textTriangles;
	public byte flags;
	byte dummy;
	public Mesh(EndianDataInputStream din, int version)throws IOException{
		din.order(ByteOrder.LITTLE_ENDIAN);
		centre=new Vertex(din);
		System.out.println(centre);
		collRadius=din.readShort();
		flags=(byte)din.read();
		dummy=(byte)din.read();
		int numVertices= din.readShort();// (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("verts: "+numVertices);
		vertices=new Vertex[numVertices];
		for(int i=0;i<numVertices;i++)
		{
			vertices[i]=new Vertex(din);
			System.out.println(vertices[i]);
		}

		short numNormals= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));

		System.out.println("numNormals: "+numNormals);
		if(numNormals>=0){
			normals=new Vertex[numNormals];
			for(int i=0;i<numNormals;i++)
			{
				normals[i]=new Vertex(din);
			}
		}
		else{
			System.out.println("lights!!!!!!!!!!!!!!!!!");
			lights=new char[-numNormals];
			for(int i=0;i<-numNormals;i++)
			{
				lights[i]=din.readChar();;
				System.out.println((int)lights[i]);
			}
		}

		int numTextRects= din.readShort();// (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		System.out.println("numTextRects: "+numTextRects);
		textRects=new MeshFace4[numTextRects];
		for(int i=0;i<numTextRects;i++)
		{
			textRects[i]=new MeshFace4(din, version,false);
			System.out.println(textRects[i]);
		}

		int numTextTriangles= din.readShort();//(din.read() & 0xff) | ((din.read() & 0xff))<<8;
		System.out.println("numTextTriangles: "+numTextTriangles);

		textTriangles=new MeshFace3[numTextTriangles];
		for(int i=0;i<numTextTriangles;i++)
		{
			textTriangles[i]=new MeshFace3(din,version,false);
			System.out.println(textTriangles[i]);
		}



		if(version==Level.TR2||version==Level.TR3){
			int numColoredRects= din.readShort();// (din.read() & 0xff) | ((din.read() & 0xff)<<8);
			System.out.println("numColoredRects: "+numTextRects);
			textRects=new MeshFace4[numColoredRects];
			for(int i=0;i<numColoredRects;i++)
			{
				textRects[i]=new MeshFace4(din, version, true);
				System.out.println(textRects[i]);
			}

			int numColoredTriangles= din.readShort();//(din.read() & 0xff) | ((din.read() & 0xff))<<8;
			System.out.println("numColoredTriangles: "+numTextTriangles);

			textTriangles=new MeshFace3[numColoredTriangles];
			for(int i=0;i<numColoredTriangles;i++)
			{
				textTriangles[i]=new MeshFace3(din, version, true);
				System.out.println(textTriangles[i]);
			}
		}

	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("centre: ");
		sb.append(centre);
		sb.append("  flags: ");
		sb.append(flags);
		return sb.toString();
	}

	
}
/*
virtual struct tr4_mesh // (variable length)
{
    tr_vertex Centre;
      int32_t CollRadius;

      int16_t NumVertices;           // Number of vertices in this mesh
    tr_vertex Vertices[NumVertices]; // List of vertices (relative coordinates)

      int16_t NumNormals;

  if(NumNormals > 0)
    tr_vertex Normals[NumNormals];
  else
      int16_t Lights[abs(NumNormals)];

     int16_t NumTexturedRectangles; // number of textured rectangles in this mesh
    tr_face4 TexturedRectangles[NumTexturedRectangles]; // list of textured rectangles

     int16_t NumTexturedTriangles;  // number of textured triangles in this mesh
    tr_face3 TexturedTriangles[NumTexturedTriangles]; // list of textured triangles
};
 * */
