package com.neoexpert.tr4.level.animation;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class Frame{
	public BoundingBox bounding_box;
	public short offsetX;
	public short offsetY;
	public short offsetZ;
	public int[] angles;
	public Frame(EndianDataInputStream din, float[][] rotations, int meshes_count)throws IOException{
		bounding_box=new BoundingBox(din);
		offsetX= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		offsetY= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		offsetZ= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		//int numAngles= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		//int numAngles= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		//int numAngles = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		

		for(int i = 0; i < meshes_count; i++){
		float[] rot = rotations[i];
		short temp1= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		float ang;
                //if(game_version >= TR_IV)
                {
                    ang = (float)(temp1 & 0x0fff);
                    ang *= 360.0f / 4096.0f;
                }
                //else
                {
                //    ang = (float)(temp1 & 0x03ff);
                //    ang *= 360.0f / 1024.0f;
                }


                switch (temp1 & 0xc000)
                {
                    case 0x4000:    // x only
                        rot[0] = ang;
                        rot[1] = 0.0f;
                        rot[2] = 0.0f;
                        break;
                    case 0x8000:    // y only
                        rot[0] = 0.0f;
                        rot[1] = ang;
                        rot[2] = 0.0f;
                        break;

                    case 0xc000:    // z only
                        rot[0] = 0.0f;
                        rot[1] = 0.0f;
                        rot[2] = ang;
                        break;

                    default:        // all three
			short temp2= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
                        rot[0] = (float)((temp1 & 0x3ff0) >> 4);
                        rot[1] = (float)(((temp1 & 0x000f) << 6) | ((temp2 & 0xfc00) >> 10));
                        rot[2] = (float)(temp2 & 0x03ff);
                        rot[0] *= 360.0f / 1024.0f;
                        rot[1] *= 360.0f / 1024.0f;
                        rot[2] *= 360.0f / 1024.0f;
                        break;
                }
		}


		/*
		angles=new int[length-9];
		for(int i=0;i<numAngles;i++){
			angles[i]= ((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		
		}
		*/
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		return sb.toString();
	}
}
/*
struct tr_anim_frame    // Variable size
{
	tr_bounding_box box; // Bounding box
	int16_t OffsetX, OffsetY, OffsetZ; // Starting offset for this model
	int16_t NumValues;
	uint16_t AngleSets[];   // Variable size
}
 * */
