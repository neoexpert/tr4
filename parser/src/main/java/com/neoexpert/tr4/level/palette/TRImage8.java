package com.neoexpert.tr4.level.palette;

import com.neoexpert.*;
import com.neoexpert.tr4.level.*;
import java.io.*;

public class TRImage8{
	//uint8_t pixels[256 * 256];
	public int[][] pixels=new int[256][256];
	public TRImage8(EndianDataInputStream din)throws IOException{
		for(int x=0;x<256;x++)
			for(int y=0;y<256;y++)
				pixels[x][y]=din.readByte()& 0xFF;

	}
}
/*
struct tr_vertex   // 6 bytes
{
    int16_t x;
    int16_t y;
    int16_t z;
};
 * */
