package com.neoexpert.tr4.level;

import com.neoexpert.*;
import com.neoexpert.tr4.level.*;
import java.io.*;

public class Vertex{
	public short x,y,z;
	public Vertex(EndianDataInputStream din)throws IOException{
		x = (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		y = (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		z = (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
	}
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(");
		sb.append(x);
		sb.append(",");
		sb.append(y);
		sb.append(",");
		sb.append(z);
		sb.append(")");
		return sb.toString();
	}
}
/*
struct tr_vertex   // 6 bytes
{
    int16_t x;
    int16_t y;
    int16_t z;
};
 * */
