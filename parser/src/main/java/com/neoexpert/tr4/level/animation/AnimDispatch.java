package com.neoexpert.tr4.level.animation;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class AnimDispatch{
	public final int low;
	public final int high;
	public final int nextAnim;
	public final int nextFrame;
	public AnimDispatch(EndianDataInputStream din)throws IOException{
		low=din.readChar();
		high=din.readChar();
		nextAnim=din.readChar();
		nextFrame=din.readChar();
	}
}
/*
struct tr_anim_dispatch    // 8 bytes
{
    int16_t Low;           // Lowest frame that uses this range
    int16_t High;          // Highest frame that uses this range
    int16_t NextAnimation; // Animation to dispatch to
    int16_t NextFrame;     // Frame offset to dispatch to
};
 * */
