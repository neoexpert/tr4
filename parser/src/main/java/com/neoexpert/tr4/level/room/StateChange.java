package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class StateChange{
	public final int state;
	public final int animdispatchesCount;
	public final int animDispatchOffset;
	
	public StateChange(EndianDataInputStream din)throws IOException{
		state=din.readChar();
		animdispatchesCount=din.readChar();
		animDispatchOffset=din.readChar();
	}
}
/*
struct tr_state_change // 6 bytes
{
    uint16_t StateID;
    uint16_t NumAnimDispatches; // number of ranges (seems to always be 1..5)
    uint16_t AnimDispatch;      // Offset into AnimDispatches[]
};
 * */
