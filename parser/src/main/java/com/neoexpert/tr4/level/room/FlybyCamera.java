package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class FlybyCamera{
	public int x;
	public int y;
	public int z;
	public int dx;
	public int dy;
	public int dz;
	public byte sequence;
	public byte index;
	public int fov;
	public int timer;
	public int roll;
	public int speed;
	public int flags;
	public int roomID;

	public FlybyCamera(EndianDataInputStream din)throws IOException{
		x=din.readInt();
		y=din.readInt();
		z=din.readInt();
		dx=din.readInt();
		dy=din.readInt();
		dz=din.readInt();

		sequence=(byte)din.read();
		index=(byte)din.read();

		fov = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		roll = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		timer = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		speed = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		flags = (din.read() & 0xff) | ((din.read() & 0xff)<<8);

		roomID=din.readInt();
	}
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("x:"+x+"\n");
		sb.append("y:"+y+"\n");
		sb.append("z:"+z+"\n");
		sb.append("dx:"+dx+"\n");
		sb.append("dy:"+dy+"\n");
		sb.append("dz:"+dz+"\n");
		sb.append("sequence:"+sequence+"\n");
		sb.append("index:"+index+"\n");
		sb.append("speed:"+speed+"\n");
		sb.append("timer:"+timer+"\n");
		return sb.toString();
	}
}
/*
struct tr4_flyby_camera  // 40 bytes
{
    int32_t x;    // Camera position
    int32_t y;
    int32_t z;
    int32_t dx;   // Camera angles (so called "look at" vector)
    int32_t dy;
    int32_t dz;

    uint8_t Sequence;
    uint8_t Index;

   uint16_t FOV;
    int16_t Roll;
   uint16_t Timer;
   uint16_t Speed;
   uint16_t Flags;

   uint32_t Room_ID;
};
 * */
