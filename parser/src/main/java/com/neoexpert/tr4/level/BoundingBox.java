package com.neoexpert.tr4.level;

import com.neoexpert.*;
import java.io.*;

public class BoundingBox{
	public int minx;
	public int maxx;
	public int miny;
	public int maxy;
	public int minz;
	public int maxz;
	public BoundingBox(EndianDataInputStream din)throws IOException{
		minx= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		maxx= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		miny= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		maxy= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		minz= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
		maxz= (short)((din.read() & 0xff) | ((din.read() & 0xff)<<8));
	}
}
