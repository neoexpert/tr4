package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class RoomVertex{
	public final Vertex vertex;
	public final int lightning;
	public final int attrs;
	public final short color;
	public RoomVertex(EndianDataInputStream din)throws IOException{
		vertex=new Vertex(din);
		lightning=din.readChar();
		attrs = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		color=din.readShort();
	}

	public int getR(){
		return ((color & 0x7C00) >> 10);
	}
	public int getG(){
		return ((color & 0x03E0) >> 5);
	}
	public int getB(){
		return ((color & 0x001F));
	}

	public boolean isWater(){
		return (attrs & 1 << 13) != 0;
	}

	public boolean isWater2(){
		return (attrs & 1 << 15) != 0;
	}

	public boolean isUnderWater(){
		return (attrs & 1 << 14) != 0;
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append(vertex);
		sb.append("color: ");
		sb.append("r:");
		sb.append(getR());
		sb.append("g:");
		sb.append(getG());
		sb.append("b:");
		sb.append(getB());
		return sb.toString();
	}
}
/*
struct tr3_room_vertex  // 12 bytes
{
     tr_vertex Vertex;
       int16_t Lighting;   // Value is ignored!
      uint16_t Attributes; // A set of flags for special rendering effects
      uint16_t Colour;     // 15-bit colour
};
 * */
