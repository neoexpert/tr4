package com.neoexpert.tr4.level.mesh;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class MeshFace3{
	public int v0,v1,v2;
	public int t;
	public int effects;
	public boolean isColored;
	public MeshFace3(EndianDataInputStream din,int version, boolean isColored)throws IOException{
		this.isColored=isColored;
		v0 = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		v1 = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		v2 = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		t = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
		if(version==Level.TR4||version==Level.TR5)
			effects = (din.read() & 0xff) | ((din.read() & 0xff)<<8);
	}
	public int getTexture(){
		return t  & ~(1 << 15);
	}
	public boolean isDoubleSided(){
		return (t & 1 << 15) != 0;
	}
	public boolean hasAlphaBlending(){
		return (effects & 1 << 0) != 0;
	}

	@Override
	public String toString(){
		return "v0: " + v0 + " - v1: " + v1 + " - v2: " + v2 + " - t: "+t;
	}
}
/*
struct tr_face4    // 12 bytes
{
    uint16_t Vertices[4];
    uint16_t TRTexture;
};
 * */
