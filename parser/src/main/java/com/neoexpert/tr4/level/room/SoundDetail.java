package com.neoexpert.tr4.level.room;
import com.neoexpert.tr4.level.*;

import com.neoexpert.*;
import java.io.*;

public class SoundDetail{
	public SoundDetail(EndianDataInputStream din)throws IOException{
		din.readChar();
		din.read();
		din.read();
		din.read();
		din.read();
		din.readChar();
	}
}
/*
struct tr3_sound_details  // 8 bytes
{
   uint16_t Sample; // (index into SampleIndices)
    uint8_t Volume;
    uint8_t Range;
    uint8_t Chance;
    uint8_t Pitch;
    int16_t Characteristics;
};
 * */
