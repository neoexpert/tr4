package com.neoexpert.tr4;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.mesh.*;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.level.animation.TRAnimation;
import com.neoexpert.tr4.level.animation.*;
import java.io.*;
import com.neoexpert.math.*;

public class Animation{
	Level level;
	Model model;

	private int time;
	private int timeMax;
	private int delta;

	private int dir;

	private int index;

	private int prev;

	private int next;

	private int overrideMask;

	private TRAnimation[] anims;
	private int first;

	private int framesCount;

	private int frameIndex;
	boolean isEnded, isPrepareToNext;
	
	public Animation(Level level, Model model){
		this.level=level;
		this.model=model;
		time = 0;                                                     
		delta = 0;
        dir = 1;                                                   
		index = -1;         
		prev = 0;                                                     
		next = 0;                                                     
		overrideMask = 0;
		first=model.animation;
		this.anims=level.levelData.animations;
		setAnim(0, 0);
	}

	public void setAnim(int animIndex, int animFrame){
		TRAnimation anim=anims[animIndex];
		isEnded     = isPrepareToNext = false;
		index=animIndex;
		prev=animIndex;
		next=anim.nextAnimation-first;
		time = (animFrame <= 0 ? -animFrame : (animFrame - anim.frameStart)) / 30;
		timeMax     = (anim.frameEnd - anim.frameStart) / 30;
		framesCount=anim.frameEnd-anim.frameStart+1;
		updateInfo();
	}

	private void updateInfo(){
		TRAnimation anim=anims[index];
		frameIndex=time*30;
		int rate=Math.max(anim.frameRate, 1);
		int fCount = (anim.frameEnd - anim.frameStart) / rate + 1;
		int fIndex = time * 30 / rate;
		int k = fIndex * anim.frameRate;
		delta = (time * 30 - k) / Math.max(1, Math.min((int)anim.frameRate, framesCount - k)); // min is because in some cases framesCount > realFramesCount / frameRate * frameRate
		int fIndexA =  fIndex % fCount,              
		fIndexB = (fIndex + 1) % fCount;
		AnimFrame frameA = getFrame(anim, fIndexA);
		int frameNext = frameIndex + 1;
		isPrepareToNext = fIndexB!=0;
		if(isPrepareToNext){
            frameNext  = anim.nextFrame;
            anim=level.levelData.animations[anim.nextAnimation];
			frameNext -= anim.frameStart;
			fIndexB=frameNext/Math.max(anim.frameRate, 1);
		}
		getCommand(anim, frameNext, null, null, null);

	}
	public static final int 
	ANIM_CMD_NONE=0,
	ANIM_CMD_OFFSET=1,
	ANIM_CMD_JUMP =2,
	ANIM_CMD_EMPTY=3,
	ANIM_CMD_KILL=4,
	ANIM_CMD_SOUND =5,                                  
	ANIM_CMD_EFFECT=6;
	public static final int ROTATE_180=0;
	private void getCommand(TRAnimation anim, int frameNext, Vec3 offset, Vec3 jump, float[] rot){
		int ptr=anim.animCommand;
		
		int acCount=anim.numAnimCommands;
		char[] cmds=level.levelData.animCommands;
		for(int i=0;i<acCount;++i){
			char cmd = cmds[ptr++];
			switch(cmd){
				case ANIM_CMD_OFFSET:
					if (offset!=null) {
                        offset.x = (float)cmds[ptr++];
                        offset.y = (float)cmds[ptr++];
                        offset.z = (float)cmds[ptr++];
                    } else
                        ptr += 3;
						break;
				case ANIM_CMD_JUMP:
					if (jump!=null) {
                        jump.y = (float)cmds[ptr++];
                        jump.z = (float)cmds[ptr++]; 
					} 
					else
                        ptr += 2;
                    break;
				case ANIM_CMD_SOUND: ptr += 2; break;
				case ANIM_CMD_EFFECT :
                    if (rot!=null) {                                                        
						int frame = cmds[ptr++] - anim.frameStart;
                        int fx    = cmds[ptr++] & 0x3FFF;
                        rot[0] = (fx == ROTATE_180 && frame == frameIndex) ? (float)Math.PI : 0.0f;
                    } else
                        ptr += 2;
						break;
			}
		}
		
	}
	
	public AnimFrame getFrame(TRAnimation anim, int index){
		int offset=
		anim.frameOffset / 2 + index * anim.frameSize;
		try{                                                            
			ByteArrayInputStream bais = new ByteArrayInputStream(level.levelData.frames);

			DataInputStream dismeshes=new DataInputStream(bais);

			EndianDataInputStream dinm = new EndianDataInputStream(dismeshes);
			dinm.skipBytes(offset);
			return new AnimFrame(dinm, anim.frameSize);
		}
		catch(IOException e){
			throw new RuntimeException(e);
		} 
	}
	
	public void playNext() {
		setAnim(next, anims[index].nextFrame);
	}
	
	public boolean setState(int state, int animIndex){
		TRAnimation anim=anims[index];
		if(state==anim.state_ID)
			return true;
		boolean exists=false;
		
		
		for(int i = 0; i<anim.numStateChanges; ++i){

            StateChange s = level.levelData.stateChanges[anim.stateChangeOffset+i];                                                                       
			if(s.state==state){                                           
				exists=true;
				for(int j = 0; j<s.animdispatchesCount; j++){
					AnimDispatch range = level.levelData.animDispatches[s.animDispatchOffset+j];
					if(anim.frameStart+frameIndex>=range.low&&anim.frameStart+frameIndex<=range.high){
						if(animIndex!=-1)
							setAnim(animIndex, -1);
						else
                            setAnim(range.nextAnim-model.animation, range.nextFrame);
						return true;
                    }
				}
			}
        }
		
		return exists;
	}
	
	public void update(){                                                   
		if(!isEnded){
			time+=dir;                                
			isEnded=time<=0.0f||time>=timeMax;
            //time=clamp(time, 0.0f, timeMax);
		}                                                             
		updateInfo();
    }
}
