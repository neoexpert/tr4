package com.neoexpert.mesh;

import com.neoexpert.math.Plane;

public class PlaneMesh {
    public static MeshBuilder genPlaneMeshXZ(Plane p, float r, float g, float b){
        MeshBuilder mb=new MeshBuilder();
        short i=0;
        for(float x=-1000;x<1000;x+=10)
            for(float z=-1000;z<1000;z+=10) {
                float y=(-p.a*x-p.c*z-p.d)/p.b;
                mb.addVertex(x,y,z,r,g,b,1,0,0);
                mb.addIndex(i);
                mb.addIndex((short) (i+1));
                i++;
            }
        return mb;
    }
    public static MeshBuilder genPlaneMeshXY(Plane p, float r, float g, float b){
        MeshBuilder mb=new MeshBuilder();
        short i=0;
        for(float x=-1000;x<1000;x+=10)
            for(float y=-1000;y<1000;y+=10) {
                float z=(-p.a*x-p.b*y-p.d)/p.c;
                mb.addVertex(x,y,z,r,g,b,1,0,0);
                mb.addIndex(i);
                mb.addIndex((short) (i+1));
                i++;
            }
        return mb;
    }
}
