package com.neoexpert.mesh;

import com.neoexpert.math.*;
import com.neoexpert.mesh.*;
import com.neoexpert.rendering.*;


public class Frustum{
	/*
	 * a1*x+b1*y+c1*z+d1>0;
	 * a2*x+b2*y+c2*z+d2>0;
	 * a3*x+b3*y+c3*z+d3>0;
	 * a4*x+b4*y+c4*z+d4>0;
	 *
	 * a5*x+b5*y+c5*z+d5>0;
	 * a6*x+b6*y+c6*z+d6>0;
	 * a7*x+b7*y+c7*z+d7>0;
	 * a8*x+b8*y+c8*z+d8>0;
	 *
	 * */
    //nbl
    public Vec3 nbl =new Vec3(0,100,200);
    //ntl
    public Vec3 ntl =new Vec3(100,100,200);
    //ntr
    public Vec3 ntr =new Vec3(100,0,200);
    //nbr
    public Vec3 nbr =new Vec3(0,0,200);

    //fbl
    public Vec3 fbl =new Vec3(0,100,20);
    //ftl
    public Vec3 ftl =new Vec3(100,100,20);
    //ftr
    public Vec3 ftr =new Vec3(100,0,20);
    //fbr
    public Vec3 fbr =new Vec3(0,0,20);

    public Vec3 v0 =new Vec3(0,0,0);
    public Vec3 v1 =new Vec3(0,0,0);
    private Plane farPlane=new Plane();
    public Plane nearPlane=new Plane();
    public Plane leftPlane=new Plane();
    public Plane rightPlane=new Plane();
    public Plane topPlane=new Plane();
    public Plane bottomPlane=new Plane();
    //private Vec3 center;

    public Frustum() {}

    public void refreshold(Camera cam){
        float far=cam.far;
        float hFar=cam.hFar;

        float wFar=cam.wFar;

        float hNear = cam.hNear;
        float wNear = cam.wNear;

        Vec3 p=new Vec3(cam.eyeX,cam.eyeY,cam.eyeZ);
        float px=cam.eyeX;
        float py=cam.eyeY;
        float pz=cam.eyeZ;
        Vec3 d=new Vec3(cam.lookX,cam.lookY,cam.lookZ).normalize();
        Vec3 right=new Vec3(cam.viewMatrix[0],cam.viewMatrix[4],cam.viewMatrix[8]).normalize();



        Vec3 fc=p.add(d.multiply(far));
        Vec3 up=d.cross(right);
        Vec3 upFar = up.multiply(hFar / 2f);

        Vec3 rightFar = right.multiply(wFar / 2f);


        ftl=fc.add(upFar).subtract(rightFar);
        ftr=fc.add(upFar).add(rightFar);
        fbl=fc.subtract(upFar).subtract(rightFar);
        fbr=fc.subtract(upFar).add(rightFar);

        Vec3 nc = p.add(d.multiply(cam.near));
        Vec3 upNear = up.multiply(hNear / 2f);

        Vec3 rightNear = right.multiply(wNear / 2f);

        ntl=nc.add(upNear).subtract(rightNear);
        ntr=nc.add(upNear).add(rightNear);
        nbl=nc.subtract(upNear).subtract(rightNear);
        nbr=nc.subtract(upNear).add(rightNear);

        farPlane=new Plane(fc,d.negate());
        nearPlane=new Plane(nc,d);

        Vec3 ra=nc.add(rightNear).subtract(p).normalize();
        leftPlane=new Plane(nbl,ra.cross(up));
        Vec3 la=nc.subtract(rightNear).subtract(p).normalize();
        rightPlane=new Plane(nbr,up.cross(la));
        Vec3 ba=nc.subtract(upNear).subtract(p).normalize();
        topPlane=new Plane(nbl,ba.cross(right));
        Vec3 ta=nc.add(upNear).subtract(p).normalize();
        bottomPlane=new Plane(ntl,right.cross(ta));

    }

    public void refresh(Camera cam){
        float far=cam.far;
        float near=cam.near;
        float hFar=cam.hFar;

        float wFar=cam.wFar;

        float hNear = cam.hNear;
        float wNear = cam.wNear;

        float px=cam.eyeX;
        float py=cam.eyeY;
        float pz=cam.eyeZ;
        //Vec3 d=new Vec3(cam.lookX,cam.lookY,cam.lookZ).normalize();
        float dx=cam.lookX;
        float dy=cam.lookY;
        float dz=cam.lookZ;
        float dlength= FMath.invSqrt(dx*dx+dy*dy+dz*dz);
        dx*=dlength;
        dy*=dlength;
        dz*=dlength;

        //Vec3 right=new Vec3(cam.viewMatrix[0],cam.viewMatrix[4],cam.viewMatrix[8]).normalize();
        float rx=cam.viewMatrix[0];
        float ry=cam.viewMatrix[4];
        float rz=cam.viewMatrix[8];
        float rlength= FMath.invSqrt(rx*rx+ry*ry+rz*rz);
        rx*=rlength;
        ry*=rlength;
        rz*=rlength;


        //Vec3 fc=new Vec3(px+dx*far,py+dy*far,pz+dz*far);//.add(new Vec3(dx*far,dy*far,dz*far));
        float fcx=px+dx*far;
        float fcy=py+dy*far;
        float fcz=pz+dz*far;
        //Vec3 up=new Vec3(dx,dy,dz).cross(new Vec3(rx,ry,rz));
        float upx = ((dy * rz) - (dz * ry));
        float upy = ((dz * rx) - (dx * rz));
        float upz = ((dx * ry) - (dy * rx));
        //Vec3 upFar = new Vec3(upx,upy,upz).multiply(hFar / 2f);
        float hhFar=hFar/2f;
        float upFarx=upx*hhFar;
        float upFary=upy*hhFar;
        float upFarz=upz*hhFar;

        //Vec3 rightFar = new Vec3(rx,ry,rz).multiply(wFar / 2f);
        float hwFar=wFar/2f;
        float rightFarx=rx*hwFar;
        float rightFary=ry*hwFar;
        float rightFarz=rz*hwFar;


        ftl=new Vec3(fcx+upFarx-rightFarx,fcy+upFary-rightFary,fcz+upFarz-rightFarz);
        ftr=new Vec3(fcx+upFarx+rightFarx,fcy+upFary+rightFary,fcz+upFarz+rightFarz);
        fbl=new Vec3(fcx-upFarx-rightFarx,fcy-upFary-rightFary,fcz-upFarz-rightFarz);
        fbr=new Vec3(fcx-upFarx+rightFarx,fcy-upFary+rightFary,fcz-upFarz+rightFarz);

        //Vec3 nc = new Vec3(px,py,pz).add(new Vec3(dx,dy,dz).multiply(near));
        float ncx=px+dx*near;
        float ncy=py+dy*near;
        float ncz=pz+dz*near;
        //Vec3 upNear = new Vec3(upx,upy,upz).multiply(hNear / 2f);
	    float hhNear=hNear/2f;
        float upNearx=upx*hhNear;
        float upNeary=upy*hhNear;
        float upNearz=upz*hhNear;

        //Vec3 rightNear = new Vec3(rx,ry,rz).multiply(wNear / 2f);
	    float hwNear=wNear/2f;
        float rightNearx=rx*hwNear;
        float rightNeary=ry*hwNear;
        float rightNearz=rz*hwNear;

        ntl=new Vec3(ncx+upNearx-rightNearx,ncy+upNeary-rightNeary,ncz+upNearz-rightNearz);
        ntr=new Vec3(ncx+upNearx+rightNearx,ncy+upNeary+rightNeary,ncz+upNearz+rightNearz);
        nbl=new Vec3(ncx-upNearx-rightNearx,ncy-upNeary-rightNeary,ncz-upNearz-rightNearz);
        nbr=new Vec3(ncx-upNearx+rightNearx,ncy-upNeary+rightNeary,ncz-upNearz+rightNearz);

        farPlane=new Plane(new Vec3(fcx,fcy,fcz),new Vec3(-dx,-dy,-dz));
        nearPlane=new Plane(new Vec3(ncx,ncy,ncz),new Vec3(dx,dy,dz));

        Vec3 ra=new Vec3(ncx+rightNearx-px,ncy+rightNeary-py,ncz+rightNearz-pz).normalize();
        leftPlane=new Plane(nbr,ra.cross(new Vec3(upx,upy,upz)));
        Vec3 la=new Vec3(ncx-rightNearx-px,ncy-rightNeary-py,ncz-rightNearz-pz).normalize();
        rightPlane=new Plane(nbl,new Vec3(upx,upy,upz).cross(la));
        Vec3 ba=new Vec3(ncx-upNearx-px,ncy-upNeary-py,ncz-upNearz-pz).normalize();
        topPlane=new Plane(nbl,ba.cross(new Vec3(rx,ry,rz)));
        Vec3 ta=new Vec3(ncx+upNearx-px,ncy+upNeary-py,ncz+upNearz-pz).normalize();
        bottomPlane=new Plane(ntl,new Vec3(rx,ry,rz).cross(ta));

        //v0=nbr;
        //v1=nbr.add(rightPlane.getNormal().multiply(10f));
    }


    public MeshBuilder build(Camera cam) {
        refresh(cam);
        //up1=n.point;
        //up2=ftr;
        //up2=n.point.add(n.getNormal().multiply(100f));

        //up1=nc;
        //Vec3 tp=new Vec3(0,0,0);
        //if(intesects(tp,100))
        //    r = 1f;
        //else r=0;

        //up2=tp;
	    //float[] mvpInverse=new float[16];
	    //Matrix.invertM(mvpInverse,0,viewProjMatrix,0);

	    //float[] p1=new float[]{-1,-1,0.0f,1};
	    //Matrix.multiplyMV(p1,0,mvpInverse,0,p1,0);
	    //frustum.nbl =new Vec4(p1[0],p1[1],p1[2],p1[3]).xyz();
	    //float[] p2=new float[]{-1,1,0.0f,1};
	    //Matrix.multiplyMV(p2,0,mvpInverse,0,p2,0);
	    //frustum.ntl =new Vec4(p2[0],p2[1],p2[2],p2[3]).xyz();

	    //float[] p3=new float[]{1,1,0.0f,1};
	    //Matrix.multiplyMV(p3,0,mvpInverse,0,p3,0);
	    //frustum.ntr =new Vec4(p3[0],p3[1],p3[2],p3[3]).xyz();

	    //float[] p4=new float[]{1,-1,0.0f,1};
	    //Matrix.multiplyMV(p4,0,mvpInverse,0,p4,0);
	    //frustum.nbr =new Vec4(p4[0],p4[1],p4[2],p4[3]).xyz();


	    //float[] p5=new float[]{-1,-1,0.999f,1};
	    //Matrix.multiplyMV(p5,0,mvpInverse,0,p5,0);
	    //frustum.fbl =new Vec4(p5[0],p5[1],p5[2],p5[3]).xyz();

	    //float[] p6=new float[]{-1,1,0.999f,1};
	    //Matrix.multiplyMV(p6,0,mvpInverse,0,p6,0);
	    //frustum.ftl =new Vec4(p6[0],p6[1],p6[2],p6[3]).xyz();

	    //float[] p7=new float[]{1,1,0.999f,1};
	    //Matrix.multiplyMV(p7,0,mvpInverse,0,p7,0);
	    //frustum.ftr =new Vec4(p7[0],p7[1],p7[2],p7[3]).xyz();

	    //float[] p8=new float[]{1,-1,0.999f,1};
	    //Matrix.multiplyMV(p8,0,mvpInverse,0,p8,0);
	    //frustum.fbr =new Vec4(p8[0],p8[1],p8[2],p8[3]).xyz();

	    return buildLined();
    }

    public boolean contains(Vec3 tp) {
        float db = topPlane.distance(tp);
        float dt = bottomPlane.distance(tp);
        float dl = rightPlane.distance(tp);
        float dr = leftPlane.distance(tp);
        return topPlane.facing(tp)&bottomPlane.facing(tp)&leftPlane.facing(tp)&rightPlane.facing(tp);
        //&nearPlane.facing(tp)&farPlane.facing(tp);
    }

    public boolean intersects(Vec3 tp,float radius) {
        radius=-radius;
        float bd = topPlane.distance(tp);
        float td = bottomPlane.distance(tp);
        float ld = leftPlane.distance(tp);
        float rd = rightPlane.distance(tp);
        return topPlane.distance(tp)>radius&bottomPlane.distance(tp)>radius&leftPlane.distance(tp)>radius&rightPlane.distance(tp)>radius
               ;// &nearPlane.distance(tp)>radius&farPlane.distance(tp)>radius;
    }

	public Frustum reduceTo(Portal p){
		if (!intersects(p.center, p.radius))
			return null;
		Frustum f= new Frustum();
		f.nbl = nbl;
		f.nbr = nbr;
		f.ntl = ntl;
		f.ntr = ntr;
		//f.ftl = ftl;
		//f.ftr = ftr;
		//f.fbl = fbl;
		//f.fbr = fbr;

		{
			Vec3 ftl1 = topPlane.intersect(f.ntl, p.l02);
			//f.ftl = nftl;
			if (ftl1.subtract(p.v0).dot(p.l02.dir) < 0)
				f.ftl = ftl1;
			else f.ftl = p.v0;

			Vec3 ftr1 = topPlane.intersect(f.ntl, p.l13);
			//f.ftr = nftr;
			if (ftr1.subtract(p.v1).dot(p.l13.dir) < 0)
				f.ftr = ftr1;
			else f.ftr = p.v1;


			Vec3 fbl1 = bottomPlane.intersect(f.nbl, p.l02);
			if (fbl1.subtract(p.v2).dot(p.l02.dir) > 0)
				f.fbl = fbl1;
			else f.fbl = p.v2;

			Vec3 fbr1 = bottomPlane.intersect(f.nbl, p.l13);
			//f.fbr = nfbr;
			if (fbr1.subtract(p.v3).dot(p.l13.dir) > 0)
				f.fbr = fbr1;
			else f.fbr = p.v3;
		}

		{
			Line l01 = new Line(f.ftl, f.ftl.subtract(f.ftr));
			Line l23 = new Line(f.fbl, f.fbl.subtract(f.fbr));

			Vec3 ftr1 = rightPlane.intersect(f.ntr, l01);
			//f.ftl = nftl;
			if (ftr1.subtract(f.ftr).dot(l01.dir) > 0)
				f.ftr = ftr1;
			else f.ftr = f.ftr;

			Vec3 fbr1 = rightPlane.intersect(f.ntr, l23);
			//f.ftr = nftr;
			if (fbr1.subtract(f.fbr).dot(l23.dir) > 0)
				f.fbr = fbr1;
			else f.fbr = f.fbr;


			Vec3 fbl1 = leftPlane.intersect(f.nbl, l23);
			if (fbl1.subtract(f.fbl).dot(l23.dir) < 0)
				f.fbl = fbl1;
			else f.fbl = f.fbl;

			Vec3 ftl1 = leftPlane.intersect(f.nbl, l01);
			//f.fbr = nfbr;
			if (ftl1.subtract(f.ftl).dot(l01.dir) < 0)
				f.ftl = ftl1;
			else f.ftl = f.ftl;
		}
		f.rebuildPlanes();
		return f;
	}

	private void rebuildPlanes()
	{
		Vec3 cv0=ftl.subtract(ntl);
		Vec3 cv1=ftr.subtract(ntr);
		Vec3 cv2=fbl.subtract(nbl);
		Vec3 cv3=fbr.subtract(nbr);
		topPlane = new Plane(ntl, cv0.cross(cv1).normalize());
		bottomPlane = new Plane(nbl, cv3.cross(cv2).normalize());
		rightPlane = new Plane(ntl, cv2.cross(cv0).normalize());
		leftPlane = new Plane(ntr, cv1.cross(cv3).normalize());
	}

    public MeshBuilder buildLined(){
	    MeshBuilder mb=new MeshBuilder();
	    createLinesIndexes(mb);
	    build(mb);
	    return mb;
    
    }
    public MeshBuilder buildFaced(){
	    MeshBuilder mb=new MeshBuilder();
	    createTrianglesIndexes(mb);
	    build(mb);
	    return mb;
    
    }
    private void build(MeshBuilder mb){
        //mb.addIndex((short) 8);
        //mb.addIndex((short) 9);


        //mb.addIndex((short) 1);
        //mb.addIndex((short) 3);

        //0
        mb.addVertex(nbl.x, nbl.y, nbl.z,1,0,0,1,1,1);
        //1
        mb.addVertex(ntl.x, ntl.y, ntl.z,0,1,0,1,1,1);
        //2
        mb.addVertex(ntr.x, ntr.y, ntr.z,0,0,1,1,1,1);
        //3
        mb.addVertex(nbr.x, nbr.y, nbr.z,0,1,1,1,1,1);

        //4
        mb.addVertex(fbl.x, fbl.y, fbl.z,0,1,1,1,1,1);
        //5
        mb.addVertex(ftl.x, ftl.y, ftl.z,0,0,1,1,1,1);
        //6
        mb.addVertex(ftr.x, ftr.y, ftr.z,0,1,0,1,1,1);
        //7
        mb.addVertex(fbr.x, fbr.y, fbr.z,1,0,0,1,1,1);

        mb.addVertex(v0.x, v0.y, v0.z,1,0,0,1,1,1);
        mb.addVertex(v1.x, v1.y, v1.z,0,0,1,1,1,1);

    }

    private void createTrianglesIndexes(MeshBuilder mb) {
        mb.addIndex((short) 0);
        mb.addIndex((short) 1);
        mb.addIndex((short) 2);
        mb.addIndex((short) 0);
        mb.addIndex((short) 2);
        mb.addIndex((short) 3);


        mb.addIndex((short) 0);
        mb.addIndex((short) 1);
        mb.addIndex((short) 5);
        mb.addIndex((short) 0);
        mb.addIndex((short) 5);
        mb.addIndex((short) 4);

        mb.addIndex((short) 3);
        mb.addIndex((short) 2);
        mb.addIndex((short) 6);
        mb.addIndex((short) 3);
        mb.addIndex((short) 6);
        mb.addIndex((short) 7);

        mb.addIndex((short) 1);
        mb.addIndex((short) 2);
        mb.addIndex((short) 5);
        mb.addIndex((short) 2);
        mb.addIndex((short) 5);
        mb.addIndex((short) 6);

        mb.addIndex((short) 0);
        mb.addIndex((short) 3);
        mb.addIndex((short) 4);
        mb.addIndex((short) 3);
        mb.addIndex((short) 7);
        mb.addIndex((short) 4);

        mb.addIndex((short) 4);
        mb.addIndex((short) 5);
        mb.addIndex((short) 6);
        mb.addIndex((short) 4);
        mb.addIndex((short) 6);
        mb.addIndex((short) 7);
    }

    private void createLinesIndexes(MeshBuilder mb) {
        mb.addIndex((short) 0);
        mb.addIndex((short) 1);
        mb.addIndex((short) 1);
        mb.addIndex((short) 2);
        mb.addIndex((short) 2);
        mb.addIndex((short) 3);
        mb.addIndex((short) 3);
        mb.addIndex((short) 0);

        mb.addIndex((short) 4);
        mb.addIndex((short) 5);
        mb.addIndex((short) 5);
        mb.addIndex((short) 6);
        mb.addIndex((short) 6);
        mb.addIndex((short) 7);
        mb.addIndex((short) 7);
        mb.addIndex((short) 4);


        mb.addIndex((short) 0);
        mb.addIndex((short) 4);

        mb.addIndex((short) 1);
        mb.addIndex((short) 5);

        mb.addIndex((short) 2);
        mb.addIndex((short) 6);

        mb.addIndex((short) 3);
        mb.addIndex((short) 7);

        mb.addIndex((short) 8);
        mb.addIndex((short) 9);
    }


}
