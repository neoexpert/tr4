package com.neoexpert.mesh;

import com.neoexpert.math.*;
import java.nio.*;
import java.util.*;

public class MeshBuilder
{
	public MeshBuilder(int meshsize, int indexsize)
	{
		positions = new ArrayList<>(meshsize);
		normals = new ArrayList<>(meshsize);
		indices = new ArrayList<>(indexsize);
		colors = new ArrayList<>();
		texcoords = new ArrayList<>();
	}

	public MeshBuilder()
	{
		positions = new ArrayList<>();
		normals = new ArrayList<>();
		indices = new ArrayList<>();
		colors = new ArrayList<>();
		texcoords = new ArrayList<>();
	}
	private final List<Vec3> positions;
	private final List<Vec3> normals;
	private final List<Vec4> colors;
	private final List<Vec2> texcoords;
	private final List<Short> indices;
	public short fi;

	public void addTexCoord(Vec2 texcoord)
	{
		texcoords.add(texcoord);
	}

	public void addColor(Vec4 color)
	{
		colors.add(color);
	}

	public void addPosition(Vec3 v)
	{
		positions.add(v);
		normals.add(new Vec3());
	}
	
	public FloatBuffer genVertices()
	{
		int verticesCount=positions.size();
		int size=
		//positions
		verticesCount*3
		//normals
		+verticesCount*3
		//colors
		+verticesCount*4
		//textcoords
		+verticesCount*2;
		float[] vertices = new float[size];
		int ix=0;
		for (int i=0; i < positions.size(); i++)
		{
			Vec3 pos= positions.get(i);
			vertices[ix++] =pos.x;
			vertices[ix++] =pos.y;
			vertices[ix++] =pos.z;
			//normal:
			Vec3 n=normals.get(i);
			vertices[ix++] =n.x;
			vertices[ix++] =n.y;
			vertices[ix++] =n.z;
			
			Vec4 color= colors.get(i);
			vertices[ix++] =color.x;
			vertices[ix++] =color.y;
			vertices[ix++] =color.z;
			vertices[ix++] =color.w;

			Vec2 textcoord=texcoords.get(i);
			vertices[ix++]=textcoord.x;
			vertices[ix++]=textcoord.y;
		}
		FloatBuffer mVertices = ByteBuffer
			.allocateDirect(vertices.length * 4)
			.order(ByteOrder.nativeOrder())
			.asFloatBuffer()
			.put(vertices);
		return mVertices;
	}
	public ShortBuffer genIndices()
	{
		short[] ret = new short[indices.size()];
		for (int i=0; i < ret.length; i++)
		{
			ret[i] = indices.get(i);
		}
		ShortBuffer drawOrder = ByteBuffer
			.allocateDirect(ret.length * 2)
			.order(ByteOrder.nativeOrder())
			.asShortBuffer()
			.put(ret);
		return drawOrder;
	}
	public void calculateNormals()
	{
		for (int i = 0; i < indices.size(); i += 3)
		{
			Vec3 v0 = positions.get(indices.get(i));
			Vec3 v1 = positions.get(indices.get(i + 1));
			Vec3 v2 = positions.get(indices.get(i + 2));

			Vec3 normal = v2.subtract(v0).cross(v1.subtract(v0)).normalize();

			normals.get(indices.get(i)).addLocal(normal);
			normals.get(indices.get(i + 1)).addLocal(normal);
			normals.get(indices.get(i + 2)).addLocal(normal);
		}
		for(int i = 0; i < normals.size(); i++)
		{
			normals.get(i).normalize();
		}
	}

	public void addIndex(short i) {
		indices.add(i);
	}

	public void addVertex(float x, float y, float z, float r, float g, float b, float a, float tx, float ty) {
		addPosition(new Vec3(x,y,z));
		addColor(new Vec4(r,g,b,a));
		addTexCoord(new Vec2(tx,ty));
	}
}
