package com.neoexpert.rendering;
import com.neoexpert.math.*;


public class Camera{
	public float near, far, aspect,fov;
	public float upX,upY=1,upZ;
	public float eyeX,eyeY,eyeZ;
	public float lookX,lookY,lookZ;
	public float[] projectionMatrix = new float[16];
	public float[] viewMatrix = new float[16];
	public float[] viewProjMatrix = new float[16];
	public float hFar;
	public float wFar;
	public float hNear;
	public float wNear;

	public void setUp(float upX, float upY, float upZ){
		this.upX=upX;
		this.upY=upY;
		this.upZ=upZ;
		refreshViewMatrix();
	}
	public void setPos(float eyeX, float eyeY, float eyeZ){
		this.eyeX=eyeX;
		this.eyeY=eyeY;
		this.eyeZ=eyeZ;
		refreshViewMatrix();
	}
	public void lookAt(float eyeX, float eyeY, float eyeZ, float lookX, float lookY, float lookZ) {
		this.eyeX=eyeX;
		this.eyeY=eyeY;
		this.eyeZ=eyeZ;
		this.lookX=lookX;
		this.lookY=lookY;
		this.lookZ=lookZ;
		refreshViewMatrix();
	}
	public void refreshViewMatrix(){
		//viewMatrix.translation(0, 0, 0);
		Matrix.setLookAtM(viewMatrix,0,eyeX, eyeY, eyeZ, eyeX+lookX, eyeY+lookY, eyeZ+lookZ, upX, upY, upZ);
		Matrix.multiplyMM(viewProjMatrix,0,viewMatrix ,0,projectionMatrix ,0);
	}
	public void rotate(float rotX, float rotY){
		lookX=(float)(Math.sin(rotX)*Math.sin(rotY));
		lookY=(float)Math.cos(rotY);
		lookZ=(float)(Math.cos(rotX)*Math.sin(rotY));
		refreshViewMatrix();
	}
	public void frustum(float ratio, float bottom, float top, float near, float far){
		Matrix.frustumM(projectionMatrix,0,ratio, -ratio, bottom, top, near, far);
		refreshViewMatrix();
	}

	public void perspective(float aspect, float fov, float near, float far){
		this.far=far;
		this.near=near;
		this.fov=fov;
		this.aspect = aspect;
		hFar= (float) (2f*Math.tan(fov)*far);
		wFar=hFar*aspect;
		hNear= (float) (2f*Math.tan(fov)*near);
		wNear=hNear*aspect;
		Matrix.perspectiveM(projectionMatrix,0,fov,aspect,near,far);
		refreshViewMatrix();
	}
	public void ortho(float left, float right, float bottom, float top, float near, float far){
		Matrix.orthoM(projectionMatrix,0,left, right, bottom, top, near, far);
		refreshViewMatrix();
	}
	public void moveForwards(float speed){
		eyeX += lookX*speed;
		eyeY += lookY*speed;
		eyeZ += lookZ*speed;
		refreshViewMatrix();
	}

	public void moveBackwards(float speed){
		eyeX -= lookX*speed;
		eyeY -= lookY*speed;
		eyeZ -= lookZ*speed;
		refreshViewMatrix();
	}
	public void moveLeftwards(float speed){
		eyeX -= lookZ*speed;
		eyeZ += lookX*speed;
		refreshViewMatrix();
	}
	public void moveRightwards(float speed){
		eyeX += lookZ*speed;
		eyeZ -= lookX*speed;
		refreshViewMatrix();
	}

}
