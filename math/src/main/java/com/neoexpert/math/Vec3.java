package com.neoexpert.math;

public class Vec3
{
	public Vec3(){

	}
	public Vec3(float x, float y, float z){
		this.x=x;
		this.y=y;
		this.z=z;
	}
	public float x;

	public float y;

	public float z;

	public void addLocal(Vec3 v)
	{
		x+=v.x;
		y+=v.y;
		z+=v.z;
	}

	public Vec3 normalize()
	{
		return divide(length());
	}

	public Vec3 divide(float v)
	{
		return new Vec3(x/v,y/v,z/v);
	}
	public Vec3 cross(Vec3 v){
        float resX = ((y * v.z) - (z * v.y)); 
        float resY = ((z * v.x) - (x * v.z));
        float resZ = ((x * v.y) - (y * v.x));
		return new Vec3(resX,resY,resZ);
	}
	public Vec3 negate(){
		return new Vec3(-x,-y,-z);
	}

	public float det(){
		return x*x+y*y+z*z;
	}

	public float length(){
		return (float)Math.sqrt(x*x+y*y+z*z);
	}

    public Vec3 add(Vec3 v) {
		return new Vec3(x+v.x,y+v.y,z+v.z);
    }
	public Vec3 subtract(Vec3 v){
		return new Vec3(x-v.x,y-v.y,z-v.z);
	}
	public Vec3 multiply(float far) {
		return new Vec3(x*far,y*far,z*far);
	}

	public float dot(Vec3 v) {
		return x*v.x+y*v.y+z*v.z;
	}
}
