package com.neoexpert.math;

public class Vec4
{
	public float x;
	public float y;
	public float z;
	public float w;

	public Vec4(float x, float y, float z, float w) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.w=w;
	}

	public Vec4(){}

    public Vec3 xyz() {
		return new Vec3(x/w,y/w,z/w);
    }
}
