package com.neoexpert.math;

public class FMath {
	//from:
	//https://stackoverflow.com/questions/11513344/how-to-implement-the-fast-inverse-square-root-in-java
	public static float invSqrt(float x) {
		float xhalf = 0.5f * x;
		int i = Float.floatToIntBits(x);
		i = 0x5f3759df - (i >> 1);
		x = Float.intBitsToFloat(i);
		x *= (1.5f - xhalf * x * x);
		return x;
	}

    public static float max(float ... v) {
	    float max=Float.NEGATIVE_INFINITY;
	    for(float f:v)
	    	if(max<f)
	    		max=f;
	    return max;
    }
}
