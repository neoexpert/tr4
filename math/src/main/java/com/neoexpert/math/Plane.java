package com.neoexpert.math;

import com.neoexpert.math.Vec3;

public class Plane {
    public float a;
    public float b;
    public float c;
    public float d;
    public Vec3 point;

    public Plane(){
        point=new Vec3();
    }
    public Plane(Vec3 point, Vec3 normal) {
        a=normal.x;
        b=normal.y;
        c=normal.z;
        this.point=point;
        d=-(normal.x*point.x+normal.y*point.y+normal.z*point.z);
    }
    public Plane(float a, float b, float c, float d){
        this.a=a;
        this.b=b;
        this.c=c;
        this.d=d;
    }

    public Plane(Vec3 p1, Vec3 p2, Vec3 p3) {
        Vec3 v1=p1.subtract(p2);
        Vec3 v2=p1.subtract(p3);
        Vec3 n=v1.cross(v2);
        a=n.x;b=n.y;c=n.z;
        d=-a*p1.x-b*p1.y-c*p1.z;
    }

    //a1*x+b1*y+c1*z-d1-a2*x-b2*y-c2*z+d2=0
    public Line intersect(Plane p2){
        // logically the 3rd plane, but we only use the normal component.
        final Vec3 p3_normal = getNormal().cross(p2.getNormal());
        final float det = p3_normal.det();

        // If the determinant is 0, that means parallel planes, no intersection.
        // note: you may want to check against an epsilon value here.
        if (det != 0.0) {
            // calculate the final (point, normal)
            Vec3 r_point = ((p3_normal.cross(p2.getNormal()).multiply(d)).add(
                    (getNormal().cross(p3_normal).multiply(p2.d)))).divide(det);
            Vec3 r_normal = p3_normal;
            return new Line(r_point,r_normal);
        }

            /*
        Vec3 n1=getNormal();
        Vec3 n2=p.getNormal();
	    Vec3 n3=n2.cross(n1);
	    float det=n3.det();
	    if(det==0){
	        //planes are parallel to each other
	        return null;
	    }
	    Vec3 p0=((n1.multiply(p.d).subtract(n2.multiply(d)))
                .cross(n3))
                .divide(det);
        Vec3 p1 = ((n3.cross(n2).multiply(d)).add(n1.cross(n3).multiply(p.d))).divide(det);

             */
	    return null;
    }

    public boolean facing(Vec3 p){
        return  +a*p.x
                +b*p.y
                +c*p.z
                +d>=0;
    }

    public float distance(Vec3 p) {
        float denom=FMath.invSqrt(a*a+b*b+c*c);
        return  (+a*p.x
                +b*p.y
                +c*p.z
                +d)/denom;
    }
    public Vec3 getNormal() {
        return new Vec3(a,b,c);
    }

    public Vec3 intersect(Vec3 planePoint,Line l){
        Vec3 n = getNormal();
        Vec3 diff = l.point.subtract(planePoint);
        float prod1 = diff.dot(n);
        float prod2 = l.dir.dot(n);
        float prod3 = prod1 / prod2;
        return l.point.subtract(l.dir.multiply(prod3));
    }
}
