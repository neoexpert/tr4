package com.neoexpert.math;


public class Line{
    public Vec3 point;
    public Vec3 dir;

    public Line(){
        point=new Vec3();
    }

    public Line(Vec3 point, Vec3 dir) {
	    this.point=point;
	    this.dir=dir;
    }
}
